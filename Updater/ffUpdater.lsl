string pin = "774542319";
integer channel = -992345;

list uuids;
list dbids;
list types;
list requestKeys;
list requestUUIDS;
string scriptKey = "nzaXOCwJMlPyR8lCNMwD";
string hostname = "http://www.feistyfelinesbreedables.com:8080/portal";
key HttpRequest(string action, list parameters, string body)
{
    return llHTTPRequest(hostname+action,[HTTP_CUSTOM_HEADER,"Script-Key", scriptKey,HTTP_CUSTOM_HEADER, "Group-id", llList2String(llGetObjectDetails(llGetKey(),[OBJECT_GROUP]),0),HTTP_BODY_MAXLENGTH,16384] + parameters ,body);
}

update(integer num)
{
    integer dbid = llList2Integer(dbids,num);
    string currentid = llList2String(uuids,num);
    string type = llList2String(types,num);
	if(type == "Cat")
	{
		llRezObject("Feisty Feline", llGetPos(), <0.0,0.0,0.0>, llEuler2Rot(<0.0,90.0,180.0> * DEG_TO_RAD), dbid);
	}
	else if (type == "Basket")
	{
		llRezObject("Feisty Felines Basket", llGetPos(), <0.0,0.0,0.0>, llEuler2Rot(<0.0,00.0,00.0> * DEG_TO_RAD), dbid);
	}
    llSay(channel,pin + currentid);
    
}

startUpdates()
{
    integer len = llGetListLength(uuids);
    if(len == llGetListLength(dbids))
    {
        llOwnerSay("Found " + (string)len + " cats to update:\n" + llDumpList2String(uuids,","));
        while(len--)
        {
            update(len);
        }
    }
	types = [];
    uuids = [];
    dbids = [];
}

default
{
    state_entry()
    {
        
    }
    
    touch_start(integer num)
    {
        if(llDetectedKey(0) == llGetOwner())
        {
            llSensor("",NULL_KEY,PASSIVE|SCRIPTED,20.0,PI);
        }
    }
    
    http_response(key request_id, integer status, list metadata, string bo)
    {
        integer req = llListFindList(requestKeys,[request_id]);
        if(req != -1 && status == 200 && bo != "-1")
        {
			if(llGetSubString(bo,0,2) == "Cat")
			{
				types += ["Cat"];
				dbids += [(integer)llGetSubString(bo,3,llStringLength(bo)-1)];
			}
			else
			{
				types += ["Basket"];
				dbids += [(integer)llGetSubString(bo,6,llStringLength(bo)-1)];
			}
            uuids += [llList2Key(requestUUIDS,req)];
        }
    }    
    
    sensor( integer detected )
    {
        llOwnerSay("Updater is searching for cats to update. Please wait.");
        while(detected--)
        {
            key object = llDetectedKey(detected);
            requestUUIDS += [object];
            requestKeys += [HttpRequest("/Cat/dbid?catid=" + (string)object,[],"")];            
        }
        llSetTimerEvent(10.0);
    }
    
    object_rez(key catid)
    {
        llGiveInventory(catid,"Feisty Felines Basket");
        llGiveInventory(catid,"Feisty Feline");
    }
    
    timer()
    {
        llSetTimerEvent(0.0);
        startUpdates();
    }
}