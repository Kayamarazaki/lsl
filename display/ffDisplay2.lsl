list types;
list sculptmap;
list pos;
list size;
list rot;
list mirror;
list stitching;
list texture;

integer hover=0;
integer bodyprim = 1;
integer headprim = 2;
integer eyesprim = 3;
integer earsprim = 4;
integer lfprim = 5;
integer rfprim = 6;
integer lrprim = 7;
integer rrprim = 8;
integer tailprim = 9;
integer neckprim = 10;
integer whiskersprim = 11;

string name;
integer male;

setText()
{
    
    string text = "Feisty Felines Display\n";
    if(male)
    {
		text += "♂";
	}
	else
	{
		text += "♀";
	}
	text += name + "\n";
	text += "Age: 5\n";
	text += "Hunger: 0%\n";
    text += "Happiness: 100%\n";
    
	text += "Heat: 80%\n";
    
	text += "Cycles Left: 6\n";
    vector color;

	color = <1.0,1.0,1.0>;
    llSetText(text,color,1.0);
}

integer getListNumber(list li, string str)
{
    integer i;
    integer l = llGetListLength(li);
    for(i =0;i<l;i++)
    {
        if(llList2String(li,i) == str)
            return i;
    }
    return -1;
}

setPrim(integer prim, string type)
{
    integer num = getListNumber(types,type);
    if(num != -1)
    {
        list params;
        vector euler = (vector)llList2String(rot,num);
        rotation ro = llEuler2Rot(euler * DEG_TO_RAD);
        vector p= (vector)llList2String(pos,num);
        vector s= (vector)llList2String(size,num);
        string primname;
        string sculptm = llList2String(sculptmap,num);
        string text = llList2String(texture,num);
        integer mirr = llList2Integer(mirror,num);
        integer stitch = llList2Integer(stitching,num);
        
        integer sculpttype = 0;
        if(mirr)
        {
            if(stitch)
            {
                sculpttype = PRIM_SCULPT_TYPE_PLANE | PRIM_SCULPT_FLAG_MIRROR;
            }
            else
            {
                sculpttype = PRIM_SCULPT_TYPE_SPHERE | PRIM_SCULPT_FLAG_MIRROR;
            }
        }
        else
        {
            if(stitch)
            {
                sculpttype = PRIM_SCULPT_TYPE_PLANE;
            }
            else
            {
                sculpttype = PRIM_SCULPT_TYPE_SPHERE ;
            }
        }
        if(prim==1)
        {
            primname = "Feisty Felines Display";
            p = llGetPos();
        }
        else
        {
            primname = type;
            vector rootEuler = (vector)llList2String(rot,getListNumber(types,"Body"));
            rotation rootRot = llEuler2Rot(rootEuler * DEG_TO_RAD);
            ro = ro / rootRot / rootRot;
            p = p/rootRot;
        }
        
        list parameters = [PRIM_NAME, primname, PRIM_ROTATION, ro, PRIM_POSITION, p, PRIM_SIZE, s, PRIM_TYPE, PRIM_TYPE_SCULPT, sculptm, sculpttype,PRIM_TEXTURE,ALL_SIDES,text,<1.0,1.0,0.0>,<0.0,0.0,0.0>,0.0];
        llSetLinkPrimitiveParamsFast(prim,parameters);
        
    }

}


useParameters()
{
    setPrim(bodyprim,"Body");
    setPrim(neckprim,"Neck");
    setPrim(headprim,"Head");
    setPrim(eyesprim,"Eyes");
    setPrim(earsprim,"Ears Upright");
    setPrim(lfprim,"LF Stand");
    setPrim(rfprim,"RF Stand");
    setPrim(lrprim,"LR Stand");
    setPrim(rrprim,"RR Stand");
    setPrim(tailprim,"Tail");
    setPrim(whiskersprim,"Whiskers");
    
    types = [];
    sculptmap = [];
    pos=[];
    size=[];
    rot=[];
    mirror=[];
    stitching=[];
    texture =[];
	if(hover)
	{
		setText();
	}
}

string str_replace(string str, string search, string replace) {
    return llDumpList2String(llParseStringKeepNulls((str = "") + str, [search], []), replace);
}

string getXmlContent(string node, string xml)
{
    string xmlContent = "";
    string st = "<" + node + ">";
    string et = "</" + node + ">";
    
    if(llSubStringIndex(xml,st) != -1 || llSubStringIndex(xml,et) != -1)
    {
        integer s = llSubStringIndex(xml,st) + llStringLength(st);
        integer e = llSubStringIndex(xml,et) -1;
        xmlContent = llGetSubString(xml,s,e);
    }  
    
    return xmlContent;
}

default
{
    link_message(integer sender, integer num, string msg, key id)
    {
		if(llGetSubString(msg,0,5)=="hover:")
        {
            hover = (integer)llGetSubString(msg,6,llStringLength(msg)-1);
        }
        if(llGetSubString(msg,0,4)=="name:")
        {
            name = llGetSubString(msg,5,llStringLength(msg)-1);
        }
        else if(llGetSubString(msg,0,3)=="sex:")
        {
            if((integer)llGetSubString(msg,4,llStringLength(msg)-1) == 1)
            {
                male = 1;
            }
            else
            {
                male = 0;
            }
        }
        else
        {
            string val = getXmlContent("types",msg);
            if(val != "")
            {
                types = llParseString2List(val,["/"],[]);
            }
            val = getXmlContent("texture",msg);
            if(val!="")
            {
                texture = llParseString2List(val,["/"],[]);
            }
            
            val = getXmlContent("sculptmap",msg);
            if(val!="")
            {
                sculptmap = llParseString2List(val,["/"],[]);
            }
            
            val = getXmlContent("pos",msg);
            if(val != "")
            {
                val = str_replace(val,"&lt;","<");
                val= str_replace(val,"&gt;",">");
                pos = llParseString2List(val,["/"],[]);
            }
            val = getXmlContent("size",msg);
            if(val != "")
            {
                val = str_replace(val,"&lt;","<");
                val= str_replace(val,"&gt;",">");
                size = llParseString2List(val,["/"],[]);
            }
            val = getXmlContent("rot",msg);
            if(val != "")
            {
                val = str_replace(val,"&lt;","<");
                val= str_replace(val,"&gt;",">");
                rot = llParseString2List(val,["/"],[]);
            }
            val = getXmlContent("mirror",msg);
            if(val != "")
            {
                mirror = llParseString2List(val,["/"],[]);
            }
            val = getXmlContent("stitching",msg);
            if(val != "")
            {
                stitching = llParseString2List(val,["/"],[]);
            }
            useParameters();
        }
    }
}