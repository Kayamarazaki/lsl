key dialogUser;
integer listenHandle;
integer channel ;
integer inDialog = 0;

 list adminlist = ["1450dde0-246e-4a2f-98f2-ed5e1a00f7b9","1b85d306-3e4a-45c6-b4a8-d57b8e210b52","104e8f31-4153-47ff-bc57-88b5c38d81e3"];    
string scriptKey = "nzaXOCwJMlPyR8lCNMwD";
string hostname = "http://www.feistyfelinesbreedables.com:8080/portal";
key requestKey;
integer type;
integer size;
integer body;
integer ears;
integer eyecolor;
integer eyeshape;
integer peltcolor;
integer pupilshape;
integer tail;
integer whiskers;
integer hover;
string name;
integer sex;

integer question;
integer question_type=0;
integer question_size=1;
integer question_body=2;
integer question_ears=3;
integer question_eyecolor=4;
integer question_eyeshape=5;
integer question_peltcolor=6;
integer question_pupilshape=7;
integer question_tail=8;
integer question_whiskers=9;
integer question_hover=10;
integer question_name=11;
integer question_sex=12;

 integer getAdmin(key toucher)
 {
    integer l = llGetListLength(adminlist);
    integer i;
    for(i = 0; i<l;i++)
    {
        if(llList2Key(adminlist,i)== toucher)
            return 1;
    }
    return 0;
 }
endDialog()
{
    inDialog=0;
    question = 0;
    llSetTimerEvent(0.0);
}

key HttpRequest(string action, list parameters, string body)
{
    return llHTTPRequest(hostname+action,[HTTP_CUSTOM_HEADER,"Script-Key", scriptKey,HTTP_CUSTOM_HEADER, "Group-id", llList2String(llGetObjectDetails(llGetKey(),[OBJECT_GROUP]),0),HTTP_BODY_MAXLENGTH,16384] + parameters ,body);
}
list getXmlContent(string node, string xml)
{
    list xmlContent = [];
    string st = "<" + node + ">";
    string et = "</" + node + ">";
    
    while(llSubStringIndex(xml,st) != -1 || llSubStringIndex(xml,et) != -1)
    {
        integer s = llSubStringIndex(xml,st) + llStringLength(st);
        integer e = llSubStringIndex(xml,et) -1;
        xmlContent += [llGetSubString(xml,s,e)];
        
        xml = llGetSubString(xml,e + llStringLength(et), llStringLength(xml));
    }
    
    
    return xmlContent;
}
default
{   
    state_entry()
    {
        channel = (integer)(llFrand(-5000.0) - 5000.0);
    }
    
    touch_start(integer num)
    {
        if(!inDialog && getAdmin(llDetectedKey(0)))
        {
            dialogUser = llDetectedKey(0);
            inDialog = 1;
            llSetTimerEvent(20.0);
            question = question_type;
            listenHandle = llListen(channel,"",dialogUser,"");
            llTextBox(dialogUser, "Please enter the cat type id.",channel);
        }
    }    
    
    listen(integer channel, string name, key id, string message)
    {
        llListenRemove(listenHandle);
        if(id == dialogUser)
        {
            if(question == question_type)
            {
                type = (integer)message;
                llSetTimerEvent(20.0);
                question = question_size;
                listenHandle = llListen(channel,"",dialogUser,"");
                llTextBox(dialogUser, "Please enter the size (1=baby,2=adolescent,3=adult).",channel);
            }
            else if (question == question_size)
            {
                size = (integer)message;
                llSetTimerEvent(20.0);
                question = question_body;
                listenHandle = llListen(channel,"",dialogUser,"");
                llTextBox(dialogUser, "Please enter the body id.",channel);
            }
            else if (question == question_body)
            {
                body = (integer)message;
                llSetTimerEvent(20.0);
                question = question_ears;
                listenHandle = llListen(channel,"",dialogUser,"");
                llTextBox(dialogUser, "Please enter the ears id.",channel);
            }
            else if (question == question_ears)
            {
                ears = (integer)message;
                llSetTimerEvent(20.0);
                question = question_eyecolor;
                listenHandle = llListen(channel,"",dialogUser,"");
                llTextBox(dialogUser, "Please enter the eye color id.",channel);
            }else if (question == question_eyecolor)
            {
                eyecolor = (integer)message;
                llSetTimerEvent(20.0);
                question = question_eyeshape;
                listenHandle = llListen(channel,"",dialogUser,"");
                llTextBox(dialogUser, "Please enter the eye shape id.",channel);
            }else if (question == question_eyeshape)
            {
                eyeshape = (integer)message;
                llSetTimerEvent(20.0);
                question = question_peltcolor;
                listenHandle = llListen(channel,"",dialogUser,"");
                llTextBox(dialogUser, "Please enter the pelt color id.",channel);
            }else if (question == question_peltcolor)
            {
                peltcolor = (integer)message;
                llSetTimerEvent(20.0);
                question = question_pupilshape;
                listenHandle = llListen(channel,"",dialogUser,"");
                llTextBox(dialogUser, "Please enter the pupil shape id.",channel);
            }else if (question == question_pupilshape)
            {
                pupilshape = (integer)message;
                llSetTimerEvent(20.0);
                question = question_tail;
                listenHandle = llListen(channel,"",dialogUser,"");
                llTextBox(dialogUser, "Please enter the tail id.",channel);
            }else if (question == question_tail)
            {
                tail = (integer)message;
                llSetTimerEvent(20.0);
                question = question_whiskers;
                listenHandle = llListen(channel,"",dialogUser,"");
                llTextBox(dialogUser, "Please enter the whiskers id.",channel);
            }else if (question == question_whiskers)
            {
                whiskers = (integer)message;
                llSetTimerEvent(20.0);
                question = question_hover;
                listenHandle = llListen(channel,"",dialogUser,"");
                llTextBox(dialogUser, "Do you want example hovertext? (1 = yes, anything else is no)",channel);
            }else if (question == question_hover)
            {
                if((integer)message == 1)
                {
                    hover = 1;
					llMessageLinked(LINK_ROOT,0,"hover:"+(string)hover,"");
                    llSetTimerEvent(20.0);
                    question = question_name;
                    listenHandle = llListen(channel,"",dialogUser,"");
                    llTextBox(dialogUser, "What name do you want the hovertext to display?",channel);
                }
                else
                {
                    hover = 0;
					llMessageLinked(LINK_ROOT,0,"hover:"+(string)hover,"");
					string request = "/Cat/custom?type="+(string)type+"&body="+(string)body+"&ears="+(string)ears+"&eyecolor="+(string)eyecolor+"&eyeshape="+(string)eyeshape+"&peltcolor="+(string)peltcolor+"&pupilshape="+(string)pupilshape+"&tail="+(string)tail+"&whiskers="+(string)whiskers+"&size="+(string)size;
                    requestKey = HttpRequest(request,[],"");
                }
				
            }else if (question == question_name)
            {
                name = message;
                llMessageLinked(LINK_ROOT,0,"name:"+name,"");
                llSetTimerEvent(20.0);
                question = question_sex;
                listenHandle = llListen(channel,"",dialogUser,"");
                llTextBox(dialogUser, "What is the gender of the cat (1 = male, anything else is female)?",channel);
            }
            else if (question == question_sex)
            {
                if((integer)message == 1)
                {
                    sex = 1;
                }
                else
                {
                    sex = 0;
                }
                llMessageLinked(LINK_ROOT,0,"sex:"+(string)sex,"");
				string request = "/Cat/custom?type="+(string)type+"&body="+(string)body+"&ears="+(string)ears+"&eyecolor="+(string)eyecolor+"&eyeshape="+(string)eyeshape+"&peltcolor="+(string)peltcolor+"&pupilshape="+(string)pupilshape+"&tail="+(string)tail+"&whiskers="+(string)whiskers+"&size="+(string)size;
                requestKey = HttpRequest(request,[],"");
            }
        }
    }
    
    http_response(key request_id, integer status, list metadata, string bo)
    {
        if(request_id == requestKey)
        {
            list li1 = getXmlContent("Parameters",bo);
            if(llGetListLength(li1) > 0)
            {
                string parameters = llList2String(li1,0);
                llMessageLinked(LINK_ROOT,0,parameters,"");
            }
        }
    }
    timer()
    {
        endDialog();
        llListenRemove(listenHandle);
        llInstantMessage(dialogUser,"Dialog expired.");
    }
}