integer initialized = 0;

float height = 0.0;


float height1;
float height2;
float height3;
integer prevAge = -1;
integer age = -1 ;
string cattype;

integer bodyprim = 1;
integer headprim = 2;
integer eyesprim = 3;
integer earsprim = 4;
integer lfprim = 5;
integer rfprim = 6;
integer lrprim = 7;
integer rrprim = 8;
integer tailprim = 9;
integer neckprim = 10;
integer whiskersprim = 11;

list types;
list sculptmap;
list pos;
list size;
list rot;
list mirror;
list stitching;
list texture;
list getSimpleParamList(integer num, string type)
{
    vector p= (vector)llList2String(pos,num);
    vector s= (vector)llList2String(size,num);
    integer mirr = llList2Integer(mirror,num);
    integer stitch = llList2Integer(stitching,num);
    integer sculpttype = 0;
    if(mirr)
    {
        if(stitch)
        {
            sculpttype = PRIM_SCULPT_TYPE_PLANE | PRIM_SCULPT_FLAG_MIRROR;
        }
        else
        {
            sculpttype = PRIM_SCULPT_TYPE_SPHERE | PRIM_SCULPT_FLAG_MIRROR;
        }
    }
    else
    {
        if(stitch)
        {
            sculpttype = PRIM_SCULPT_TYPE_PLANE;
        }
        else
        {
            sculpttype = PRIM_SCULPT_TYPE_SPHERE ;
        }
    }
    return [PRIM_NAME, type, PRIM_ROTATION, (vector)llList2String(rot,num), PRIM_POSITION, p, PRIM_SIZE, s, PRIM_TYPE, PRIM_TYPE_SCULPT, llList2String(sculptmap,num), sculpttype,PRIM_TEXTURE,ALL_SIDES,llList2String(texture,num),<1.0,1.0,0.0>,<0.0,0.0,0.0>,0.0];
}
integer getListNumber(list li, string str)
{
    integer i;
    integer l = llGetListLength(li);
    for(i =0;i<l;i++)
    {
        if(llList2String(li,i) == str)
            return i;
    }
    return -1;
}

setPrim(integer prim, string type)
{
    integer num = getListNumber(types,type);
    if(num != -1)
    {
        list params;
        vector euler = (vector)llList2String(rot,num);
        rotation ro = llEuler2Rot(euler * DEG_TO_RAD);
        vector p= (vector)llList2String(pos,num);
        vector s= (vector)llList2String(size,num);
        string primname;
        string sculptm = llList2String(sculptmap,num);
        string text = llList2String(texture,num);
        integer mirr = llList2Integer(mirror,num);
        integer stitch = llList2Integer(stitching,num);
        
        integer sculpttype = 0;
        if(mirr)
        {
            if(stitch)
            {
                sculpttype = PRIM_SCULPT_TYPE_PLANE | PRIM_SCULPT_FLAG_MIRROR;
            }
            else
            {
                sculpttype = PRIM_SCULPT_TYPE_SPHERE | PRIM_SCULPT_FLAG_MIRROR;
            }
        }
        else
        {
            if(stitch)
            {
                sculpttype = PRIM_SCULPT_TYPE_PLANE;
            }
            else
            {
                sculpttype = PRIM_SCULPT_TYPE_SPHERE ;
            }
        }
        if(prim==1)
        {
            primname = "Feisty Felines " + cattype;
            vector currentPos = llGetPos();
            currentPos.z = currentPos.z + height;
            p = currentPos;
            llMessageLinked(LINK_ROOT,1,"newz:" + (string)p.z,"");
        }
        else
        {
            primname = type;
            vector rootEuler = (vector)llList2String(rot,getListNumber(types,"Body"));
            rotation rootRot = llEuler2Rot(rootEuler * DEG_TO_RAD);
            ro = ro / rootRot / rootRot;
            p = p/rootRot;
        }
        
        list parameters = [PRIM_NAME, primname, PRIM_ROTATION, ro, PRIM_POSITION, p, PRIM_SIZE, s, PRIM_TYPE, PRIM_TYPE_SCULPT, sculptm, sculpttype,PRIM_TEXTURE,ALL_SIDES,text,<1.0,1.0,0.0>,<0.0,0.0,0.0>,0.0];
        
        llSetLinkPrimitiveParamsFast(prim,parameters);
        
    }

}
messagePrimParameter(string type)
{
    list params;
    if(llStringLength(type) == 2)
    {
        integer num = getListNumber(types, type + " Back");
        
        if(num != -1)
        {
            params += getSimpleParamList(num,type);
        }
        num = getListNumber(types, type + " Front");
        
        if(num != -1)
        {
            params += getSimpleParamList(num,type);
        }
        num = getListNumber(types, type + " Stand");
        
        if(num != -1)
        {
            params += getSimpleParamList(num,type);
        }
    
    }
    else if (type = "Ears")
    {
        integer num = getListNumber(types, type + " Upright");
        if(num != -1)
        {
            params += getSimpleParamList(num,type);
        }
        num = getListNumber(types, type + " Drooped");
        if(num != -1)
        {
            params += getSimpleParamList(num,type);
        }
    }    
    llMessageLinked(LINK_ROOT,1,llDumpList2String(params,"/"),"");
}

useParameters()
{
    llMessageLinked(LINK_ROOT,1,"wait","");
	llSleep(1.0);
    setPrim(bodyprim,"Body");
    setPrim(neckprim,"Neck");
    setPrim(headprim,"Head");
    setPrim(eyesprim,"Eyes");
    setPrim(earsprim,"Ears Upright");
    setPrim(lfprim,"LF Stand");
    setPrim(rfprim,"RF Stand");
    setPrim(lrprim,"LR Stand");
    setPrim(rrprim,"RR Stand");
    setPrim(tailprim,"Tail");
    setPrim(whiskersprim,"Whiskers");
    messagePrimNumbers();
    messagePrimParameters();
    
    types = [];
    sculptmap = [];
    pos=[];
    size=[];
    rot=[];
    mirror=[];
    stitching=[];
    texture =[];
    llMessageLinked(LINK_ROOT,1,"continue","");
}

messagePrimNumbers()
{
    llMessageLinked(LINK_ROOT,1,"lfprim:"+(string)lfprim,"");
    llMessageLinked(LINK_ROOT,1,"rfprim:"+(string)rfprim,"");
    llMessageLinked(LINK_ROOT,1,"lrprim:"+(string)lrprim,"");
    llMessageLinked(LINK_ROOT,1,"rrprim:"+(string)rrprim,"");
    llMessageLinked(LINK_ROOT,1,"earsprim:"+(string)earsprim,"");
}

messagePrimParameters()
{
    messagePrimParameter("LF");
    messagePrimParameter("RF");
    messagePrimParameter("LR");
    messagePrimParameter("RR");
    messagePrimParameter("Ears");
}

string getXmlContent(string node, string xml)
{
	string xmlContent = "";
	string st = "<" + node + ">";
	string et = "</" + node + ">";
	
	if(llSubStringIndex(xml,st) != -1 || llSubStringIndex(xml,et) != -1)
	{
		integer s = llSubStringIndex(xml,st) + llStringLength(st);
		integer e = llSubStringIndex(xml,et) -1;
		xmlContent = llGetSubString(xml,s,e);
	}  
	
	return xmlContent;
}
string str_replace(string str, string search, string replace) {
    return llDumpList2String(llParseStringKeepNulls((str = "") + str, [search], []), replace);
}

default
{   
    on_rez(integer params)
    {
        initialized = 0;
    }
    link_message(integer sender, integer num, string msg, key id)
    {
        if(num ==100 && msg == "init")
        {
            initialized = 1;
        }
        if(num == 2)    
        {
            if(llGetSubString(msg,0,3) == "age:")
            {
                prevAge = age;
                age = (integer)llGetSubString(msg,4,llStringLength(msg)-1);
            }
            else if(llGetSubString(msg,0,4) == "type:")
            {
                cattype = llGetSubString(msg,5,llStringLength(msg)-1);
            }
            else
            {
                integer paramsset=0;
                string val = getXmlContent("types",msg);
                if(val != "")
                {
                    types = llParseString2List(val,["/"],[]);
                }
                
                val = getXmlContent("texture",msg);
                if(val!="")
                {
                    texture = llParseString2List(val,["/"],[]);
                }
                
                val = getXmlContent("sculptmap",msg);
                if(val!="")
                {
                    sculptmap = llParseString2List(val,["/"],[]);
                }
                
                val = getXmlContent("pos",msg);
                if(val != "")
                {
                    val = str_replace(val,"&lt;","<");
                    val= str_replace(val,"&gt;",">");
                    pos = llParseString2List(val,["/"],[]);
                }
                val = getXmlContent("size",msg);
                if(val != "")
                {
                    val = str_replace(val,"&lt;","<");
                    val= str_replace(val,"&gt;",">");
                    size = llParseString2List(val,["/"],[]);
                }
                val = getXmlContent("rot",msg);
                if(val != "")
                {
                    val = str_replace(val,"&lt;","<");
                    val= str_replace(val,"&gt;",">");
                    rot = llParseString2List(val,["/"],[]);
                }
                val = getXmlContent("mirror",msg);
                if(val != "")
                {
                    mirror = llParseString2List(val,["/"],[]);
                }
                val = getXmlContent("stitching",msg);
                if(val != "")
                {
                    stitching = llParseString2List(val,["/"],[]);
                }
                val = getXmlContent("height1",msg);
                if(val != "")
                {
                    height1 = (float)val;
                }
                val = getXmlContent("height2",msg);
                if(val != "")
                {
                    height2 = (float)val;
                }
                val = getXmlContent("height3",msg);
                if(val != "")
                {
                    height3 = (float)val;
                }
                
                if(llGetListLength(types) > 0 && llGetListLength(pos) > 0 && llGetListLength(size) > 0 && llGetListLength(rot) > 0 && llGetListLength(mirror) > 0 && llGetListLength(stitching) > 0)
                {
                    if(age < 3)
                    {
                        llMessageLinked(LINK_ROOT,3,"height"+(string)height1,"");
                    }
                    else if (age < 5)
                    {
                        llMessageLinked(LINK_ROOT,3,"height"+(string)height2,"");
                    }
                    else
                    {
                        llMessageLinked(LINK_ROOT,3,"height"+(string)height3,"");
                    }
                    if(prevAge != age)
                    {
                        if(prevAge == -1)
                        {
                            height = 0;
                            useParameters();
                            paramsset = 1;
                        }else if(prevAge < 3)
                        {
                            if(age >= 5)
                            {
                                height = height3 - height1;
                                useParameters();
                                paramsset = 1;
                            }
                            else if(age >=3)
                            {
                                height = height2 - height1;
                                useParameters();
                                paramsset = 1;
                            }
                        }
                        else if(prevAge < 5 && age >= 5)
                        {
                            height = height3 - height2;
                        }
                        
                    }
                    if(!initialized)
                    {
                        llMessageLinked(LINK_ROOT,0,"paramsReady","");
                        if(!paramsset)
                        {
                            height = 0;
                            useParameters();
                        }
                    }
                }
            }
        }
    }
}