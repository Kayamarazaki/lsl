string scriptKey = "nzaXOCwJMlPyR8lCNMwD";
string hostname = "http://www.feistyfelinesbreedables.com:8080/portal";
float height;
integer dbid;
integer birthOn = 0;
key HttpRequest(string action, list parameters, string body)
{
    return llHTTPRequest(hostname+action,[HTTP_CUSTOM_HEADER,"Script-Key", scriptKey,HTTP_CUSTOM_HEADER, "Group-id", llList2String(llGetObjectDetails(llGetKey(),[OBJECT_GROUP]),0),HTTP_BODY_MAXLENGTH,16384] + parameters ,body);
}
integer IsInteger(string var)
{
    integer i;
    for (i=0;i<llStringLength(var);++i)
    {
        if(!~llListFindList(["1","2","3","4","5","6","7","8","9","0"],[llGetSubString(var,i,i)]))
        {
            return FALSE;
        }
    }
    return TRUE;
}
default
{
    link_message(integer sender, integer num, string msg, key id)
    {
        if(num==3 || num == 100)
        {
			if(!ispet)
			{
				if(msg == "breed")
				{	
					llSensor("",NULL_KEY,PASSIVE|SCRIPTED,10.0,PI);
				}
				else if (msg == "birth=off")
				{
					birthOn = 0;
				}
				else if (msg == "birth=on")
				{
					birthOn = 1;
				}
				else if(IsInteger(msg))
				{
					
					if(birthOn)
					{
						llSetTimerEvent(10.0);
						llRezObject("Feisty Felines Basket", llGetPos() + <0.0,0.0,-1.0 * height>, <0.0,0.0,0.0>, <0.0,0.0,0.0,1.0>, (integer)msg);
					}
					else
					   llMessageLinked(LINK_ROOT,0,msg,""); 
				}
				else if (llGetSubString(msg,0,7)=="userbaby")
				{
					llSetTimerEvent(10.0);
					llRezObject("Feisty Felines Basket", llGetPos() + <0.0,0.0,-1.0 * height>, <0.0,0.0,0.0>, <0.0,0.0,0.0,1.0>, (integer)llGetSubString(msg,8,llStringLength(msg)-1));
				}
				else if (llSubStringIndex(msg,"height") != -1)
				{
					height = (float)llGetSubString(msg,6,llStringLength(msg)-1);
				}
			}
            
            if (llGetSubString(msg,0,4)=="dbid:")
            {
                dbid = (integer)llGetSubString(msg,5,llStringLength(msg)-1);
            }
			else if (llGetSubString(msg,0,3)=="pet:")
            {
                ispet = 1;
            }
            else if (llGetSubString(msg,0,6)=="notpet:")
            {
                ispet = 0;
            }
        }
    }
    
    timer()
    {
        llMessageLinked(LINK_ROOT,0,"babiesready","");
    }
    
    object_rez(key id)
    {
        llSetTimerEvent(0.0);
        llGiveInventory(id,"Feisty Feline");
        llGiveInventory(id,"Feisty Felines Basket");
    }
    
    sensor(integer detected)
    {
        while(detected--)
        {
            HttpRequest("/Breeding/try?id="+(string)llDetectedKey(detected)+"&dbid="+(string)dbid,[],"");
        }
    }
}
