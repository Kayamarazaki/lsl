integer moving = 0;
integer movementReady = 0;
integer traitsReady = 0;
integer paramsReady = 0;
integer retries = 0;
integer channel ;
integer listenHandle;
integer inDialog = 0;

integer birthOn = 0;
integer male = 0;
integer babyReady = 0;
integer soundOn = 1;



key dialogUser;
key healrequest;
key mate;
integer breeding;

integer dbid;
integer babyid;
integer breedingall = 0;
integer breedinggroup = 1;
integer breedingowner = 2;
integer breedingmate = 3;
string name ;
list meows = ["8261b789-d48b-3da3-ef66-3d2e943412b4","03f45d24-cd02-299d-9bcb-fa0592adb162"];
 list adminlist = ["1450dde0-246e-4a2f-98f2-ed5e1a00f7b9","1b85d306-3e4a-45c6-b4a8-d57b8e210b52","104e8f31-4153-47ff-bc57-88b5c38d81e3"];    
string scriptKey = "nzaXOCwJMlPyR8lCNMwD";
string hostname = "http://www.feistyfelinesbreedables.com:8080/portal";
key registerKey;
key initKey;
key pawPointsKey;
key traitsKey;

integer hovername;
integer hoverage;
integer hoverneeds;
integer hoverbreeding;
integer sick = 0;
integer healthpacks = 0;
integer petpotions;
integer ispet = 0;

integer question = 0;
integer question_movement = 1;
integer question_movement_custom = 2;
integer question_paw_points = 3;
integer question_settings = 4;
integer question_breeding = 5;
integer question_hovertext = 6;
integer question_birth = 7;
integer question_name = 8 ;
integer question_main = 9;
integer question_sound = 10;

triggerSound()
{
    if(soundOn)
    {
        llSetTimerEvent(llFrand(30.0) + 30.0);
    }
    else
    {
        llSetTimerEvent(0.0);
    }
}

key HttpRequest(string action, list parameters, string body)
{
    return llHTTPRequest(hostname+action,[HTTP_CUSTOM_HEADER,"Script-Key", scriptKey,HTTP_CUSTOM_HEADER, "Group-id", llList2String(llGetObjectDetails(llGetKey(),[OBJECT_GROUP]),0),HTTP_BODY_MAXLENGTH,16384] + parameters ,body);
}
integer isInteger(string var)
{
    integer i;
    for (i=0;i<llStringLength(var);++i)
    {
        if(!~llListFindList(["1","2","3","4","5","6","7","8","9","0"],[llGetSubString(var,i,i)]))
        {
            return FALSE;
        }
    }
    return TRUE;
}
list getXmlContent(string node, string xml)
{
    list xmlContent = [];
    string st = "<" + node + ">";
    string et = "</" + node + ">";
    
    while(llSubStringIndex(xml,st) != -1 || llSubStringIndex(xml,et) != -1)
    {
        integer s = llSubStringIndex(xml,st) + llStringLength(st);
        integer e = llSubStringIndex(xml,et) -1;
        xmlContent += [llGetSubString(xml,s,e)];
        
        xml = llGetSubString(xml,e + llStringLength(et), llStringLength(xml));
    }
    
    
    return xmlContent;
}




endDialog()
{
    inDialog=0;
    question = 0;
    triggerSound();
    llSetTimerEvent(0.0);
}

 integer getAdmin(key toucher)
 {
    integer l = llGetListLength(adminlist);
    integer i;
    for(i = 0; i<l;i++)
    {
        if(llList2Key(adminlist,i)== toucher)
            return 1;
    }
    return 0;
 }

showMainMenu()
{
    llSetTimerEvent(20.0);
    question = question_main;
    listenHandle = llListen(channel, "", dialogUser,"");
    string m = "Feisty Felines Cat Menu";
    list options = [];
    options += ["Traits"];
	if(!ispet)
	{
		options += ["Parents"];
	}
    if(dialogUser == llGetOwner())
    {
		options += ["Change Name", "Set Home", "Movement", "Settings"];
		if(!ispet)
		{
			options += ["Breeding", "Paw Points"];
			if(babyReady)
			{
				m += "\nA baby is ready to be dropped!";
				options += ["Drop Basket"];
			}
			if(sick && healthpacks > 0)
			{
				m += "\nHealthpacks: " + (string)healthpacks;
				options += ["Heal"];
			}
			if(petpotions > 0 && !sick)
			{
				m += "\nPet Potions: " + (string)petpotions;
				options += ["Make Pet"];
			}
		}
    }
    llDialog(dialogUser, m, options, channel);
}
integer readyToInit()
{
    return llGetInventoryType("Feisty Feline") != INVENTORY_NONE && llGetInventoryType("Feisty Felines Basket") != INVENTORY_NONE;
}
integer isReady()
{
    
    return (traitsReady && movementReady&&paramsReady);
}

default
{
    on_rez(integer params)
    {
        if(params != 0)
            dbid = params;
    } 

    
    state_entry()
    {   
        channel = (integer)(llFrand(-5000.0) - 5000.0);
        llAllowInventoryDrop(1);
        llSetTimerEvent(1.0);
    }
    
    timer()
    {
        retries++;
        if(readyToInit())
        {
            llSetTimerEvent(0.0);
            state init;
        }
        else if (retries == 100)
        {
            llOwnerSay("Initializing failed");
            llSetTimerEvent(0.0);
        }
    }
}

state init
{
    on_rez(integer params)
    {
        state default;
    }
    
    touch_start(integer num)
    {
        if(getAdmin(llDetectedKey(0)))
        {
            dialogUser = llDetectedKey(0);
            llSetTimerEvent(20.0);
            inDialog = 1;
            listenHandle = llListen(channel,"",dialogUser,"");
            llTextBox(dialogUser, "Please enter the dbid this cat should represent.",channel);  
        }
    }
    
    state_entry()
    {
        llAllowInventoryDrop(0);
        llMessageLinked(LINK_ROOT,4,"go","");
        llSetTimerEvent(1.0);
    }
    
    listen(integer channel, string name, key id, string message)
    {
        llListenRemove(listenHandle);
        
        if(isInteger(message))
        {
            llMessageLinked(LINK_ROOT,100,"dbid:"+message,"");
            dbid = (integer)message;
            endDialog();
            state default;
        }
    }
    
    timer()
    {
        retries++;
        if(isReady())
        {
            llSetTimerEvent(0.0);
            state initialized;
        }
        else if (retries == 100)
        {
            llOwnerSay("Initializing failed");
            llSetTimerEvent(0.0);
        }
    }
    
        
    link_message(integer sender, integer num, string msg, key id)
    {
        if(num == 0 || num == 100)
        {
            if(msg == "movementReady")
            {
                movementReady=1;
            }
            else if(msg == "traitsReady")
            {
                traitsReady=1;
            }
            else if(msg == "paramsReady")
            {
                paramsReady=1;
            }
            else if(msg=="hoverbreeding=off")
            {
                hoverbreeding = 0;
            } else if(msg=="hoverbreeding=on")
            {
                hoverbreeding = 1;
            }
            else if(msg=="hoverage=off")
            {
                hoverage = 0;
            } else if(msg=="hoverage=on")
            {
                hoverage = 1;
            }
            else if(msg=="hovername=off")
            {
                hovername = 0;
            } else if(msg=="hovername=on")
            {
                hovername = 1;
            }
            else if(msg=="hoverneeds=off")
            {
                hoverneeds = 0;
            } else if(msg=="hoverneeds=on")
            {
                hoverneeds = 1;
            }else if (msg == "birth=off")
            {
                birthOn = 0;
            }
            else if (msg == "birth=on")
            {
                birthOn = 1;
            }
            else if(msg=="breeding=everyone")
            {
                breeding = breedingall;
            }
            else if (msg=="breeding=group")
            {
                breeding = breedinggroup;
            }
            else if (msg=="breeding=owner")
            {
                breeding=breedingowner;
            }
            else if (msg == "gender=male")
            {
                male = 1;
            }
            else if (msg=="gender=female")
            {
                male=0;
            }
            else if (llGetSubString(msg,0,4)=="dbid:")
            {
                dbid = (integer)llGetSubString(msg,5,llStringLength(msg)-1);
            }
            else if (llGetSubString(msg,0,4)=="mate=")
            {
                breeding = breedingmate;
                mate = (key)llGetSubString(msg,5,llStringLength(msg)-1);
            }
            else if (llGetSubString(msg,0,4)=="name:")
            {
                name = llGetSubString(msg,5,llStringLength(msg)-1);
            }
            else if (llGetSubString(msg,0,4)=="sick:")
            {
                sick = 1;
                healthpacks = (integer)llGetSubString(msg,5,llStringLength(msg)-1);
            }
            else if (llGetSubString(msg,0,7)=="notsick:")
            {
                sick = 0;
                healthpacks = (integer)llGetSubString(msg,8,llStringLength(msg)-1);
            }
			else if (llGetSubString(msg,0,3)=="pet:")
            {
                ispet = 1;
                petpotions = (integer)llGetSubString(msg,4,llStringLength(msg)-1);
            }
            else if (llGetSubString(msg,0,6)=="notpet:")
            {
                ispet = 0;
                petpotions = (integer)llGetSubString(msg,7,llStringLength(msg)-1);
            }
            else if (isInteger(msg))
            {
                babyReady = 1;
                babyid = (integer)msg;
            }
        }
    }
}

state initialized
{
    on_rez(integer params)
    {   
        state default;
    }
    
    link_message(integer sender, integer num, string msg, key id)
    {
        if(num == 0 || num == 100)
        {
            if(msg=="hoverbreeding=off")
            {
                hoverbreeding = 0;
            } 
            else if(msg=="hoverbreeding=on")
            {
                hoverbreeding = 1;
            }
            else if(msg=="hoverage=off")
            {
                hoverage = 0;
            } else if(msg=="hoverage=on")
            {
                hoverage = 1;
            }
            else if(msg=="hovername=off")
            {
                hovername = 0;
            } else if(msg=="hovername=on")
            {
                hovername = 1;
            }
            else if(msg=="hoverneeds=off")
            {
                hoverneeds = 0;
            } else if(msg=="hoverneeds=on")
            {
                hoverneeds = 1;
            } else if (msg == "birth=off")
            {
                birthOn = 0;
            }
            else if (msg == "birth=on")
            {
                birthOn = 1;
            }
            else if(msg=="breeding=everyone")
            {
                breeding = breedingall;
            }
            else if (msg=="breeding=group")
            {
                breeding = breedinggroup;
            }
            else if (msg=="breeding=owner")
            {
                breeding=breedingowner;
            }
            else if (llGetSubString(msg,0,4)=="dbid:")
            {
                dbid = (integer)llGetSubString(msg,5,llStringLength(msg)-1);
            }
            else if (isInteger(msg))
            {
                babyReady = 1;
                babyid = (integer)msg;
            }
            else if (llGetSubString(msg,0,4)=="mate=")
            {
                breeding = breedingmate;
                mate = (key)llGetSubString(msg,5,llStringLength(msg)-1);
            }
            else if (llGetSubString(msg,0,4)=="sick:")
            {
                sick = 1;
                healthpacks = (integer)llGetSubString(msg,5,llStringLength(msg)-1);
            }
            else if (llGetSubString(msg,0,4)=="name:")
            {
                name = llGetSubString(msg,5,llStringLength(msg)-1);
            }
            else if (llGetSubString(msg,0,7)=="notsick:")
            {
                sick = 0;
                healthpacks = (integer)llGetSubString(msg,8,llStringLength(msg)-1);
            }
			else if (llGetSubString(msg,0,3)=="pet:")
            {
                ispet = 1;
                petpotions = (integer)llGetSubString(msg,4,llStringLength(msg)-1);
            }
            else if (llGetSubString(msg,0,6)=="notpet:")
            {
                ispet = 0;
                petpotions = (integer)llGetSubString(msg,7,llStringLength(msg)-1);
            }
        }
    }
    
    state_entry()
    {
        llAllowInventoryDrop(0);
        llSetObjectDesc((string)dbid);
        llSetObjectName(name);
        triggerSound();
        llMessageLinked(LINK_ROOT,100,"init","");
    }
    
    touch_start(integer num)
    {        
        if (!inDialog)
        {
            dialogUser = llDetectedKey(0);
            showMainMenu();
        }
    }
    
    timer()
    {
        if(inDialog)
        {
            endDialog();
            llListenRemove(listenHandle);
            llInstantMessage(dialogUser,"Dialog expired.");
        }
        else
        {
            
            string sound ;
            if(llFrand(1.0) < 0.5)
            {
                sound = llList2String(meows,0);
            }
            else
            {
                sound = llList2String(meows,1);
            }
            llPlaySound( sound ,1.0);
            triggerSound();
        }
    }
    
    listen(integer channel, string name, key id, string message)
    {
        llListenRemove(listenHandle);
        
        if(dialogUser==llGetOwner())
        {
            if(question == question_main)
            {
                if(message == "Set Home")
                {
                    llMessageLinked(LINK_ROOT,1,"setHome","");
                    inDialog = 0;
                    llSetTimerEvent(0.0);
                }
                else if (message=="Drop Basket")
                {
                    llMessageLinked(LINK_ROOT,3,"userbaby" + (string)babyid,"");
                    babyReady = 0;
                    endDialog();
                }
                if(message == "Traits")
                {
                    traitsKey = HttpRequest("/Cat/traits?type=cat&dbid="+(string)dbid,[],"");
                    endDialog();
                }
                else if(message == "Parents")
                {
                    traitsKey = HttpRequest("/Cat/parenttraits?type=cat&dbid="+(string)dbid,[],"");
                    endDialog();
                }
                else if (message == "Change Name")
                {
                    question = question_name;
                    
                    llSetTimerEvent(20.0);
                    listenHandle = llListen(channel,"",dialogUser,"");
                    llTextBox(dialogUser, "Please enter a name. 24 characters maximum.", channel);
                }
                else if(message == "Movement")
                {
                    llSetTimerEvent(20.0);
                    listenHandle = llListen(channel,"",dialogUser,"");
                    question = question_movement;
                    if(moving)
                    {
                        llDialog(dialogUser, "Choose movement range\nCurrent movement: On", ["5m","10m","15m","25m","Move Off", "Custom", "Back"], channel);
                    }
                    else
                    {
                        llDialog(dialogUser, "Choose movement range\nCurrent movement: Off", ["5m","10m","15m","25m","Move On", "Custom", "Back"], channel);
                    } 
                }
                else if (message == "Heal")
                {
                    if(healthpacks > 0 && sick)
                    {
                        healrequest = HttpRequest("/Cat/heal?dbid="+(string)dbid,[],"");
                    }
                }
                else if (message == "Breeding")
                {
                    llSetTimerEvent(20.0);
                    listenHandle = llListen(channel,"",dialogUser,"");     
                    question = question_breeding;
                    list options = [];
                    string m;
                    if(breeding == breedingall)
                    {
                        m = "Change your breeding settings here. (Current: Everyone)";
                        options += ["Owner Only", "Group Only","Birth"];
                    }
                    else if (breeding == breedinggroup)
                    {
                        m = "Change your breeding settings here. (Current: Group Only)";
                        options += ["Owner Only", "Everyone","Birth"];
                    }
                    else if (breeding == breedingowner)
                    {
                        m = "Change your breeding settings here. (Current: Owner Only)";
                        options += ["Group Only", "Everyone","Birth"];
                    }
                    
                    if(!male)
                    {
                        if(breeding!= breedingmate)
                        {
                            options += ["Set Partner"];
                        }
                        else
                        {
                            m = "Your cat is currently set to a mate:\n" + (string)mate + "\nTo remove it, choose one of the breeding options";
                            options += ["Owner Only", "Group Only", "Everyone", "Birth"];
                        }
                    }
                    options += ["Back"];
                    llDialog (dialogUser, m, options, channel);
                }
                else if (message == "Settings")
                {
                    llSetTimerEvent(20.0);
                    listenHandle = llListen(channel,"",dialogUser,"");     
                    question = question_settings;
                    llDialog(id, "Settings menu", ["Hovertext", "Sounds", "Return home", "Back"], channel );  //Future: animations, sounds
                    // This menu requires text above the options to display if setting is on/off.
                }
                else if (message == "Paw Points")
                {
                    llSetTimerEvent(20.0);
                    listenHandle = llListen(channel,"",dialogUser,"");     
                    question = question_paw_points;
                    llDialog(id, "Do you want to send this cat in for Paw Points?", ["Yes", "No", "Back"], channel);
                } 
                else if (message == "Help manual")
                {
                    endDialog();
                }
                else if (message == "Get HUD")
                {
                    endDialog();
                }
            }
            else if (question == question_sound)
            {
                if(message == "On")
                {
                    soundOn = 1;
                    triggerSound();
                    endDialog();
                }
                else if (message == "Off")
                {
                    soundOn = 0;
                    triggerSound();
                    endDialog();
                }
                else if (message == "Back")
                {
                    showMainMenu();
                }
            }
            else if (question == question_name)
            {
                if(llStringLength(message) <= 24)
                {
                    name = message;
                    llMessageLinked(LINK_ROOT,100,"name:" + name,"");
                    HttpRequest("/Cat/Name?dbid="+(string)dbid,[HTTP_CUSTOM_HEADER,"CatName", name],"");
                    llSetObjectName(name);
                    endDialog();
                }
                else
                {
                    llInstantMessage(dialogUser,"Name is too long.");
                    endDialog();
                }
            }
            else if (question == question_breeding)
            {
                if(message == "Set Partner")
                {
                    llSetTimerEvent(20.0);
                    listenHandle = llListen(channel,"",dialogUser,"");     
                    llTextBox(dialogUser, "Please input a male cat's ID (You can find it in the cat's description).",channel);
                }
				else if(message=="Birth")
                {
                    llSetTimerEvent(20.0);
                    listenHandle = llListen(channel,"",dialogUser,"");     
                    question = question_birth;
                    if(birthOn)
                        llDialog(id, "Do you want to turn automatic dropping of baskets On or Off (Current Setting: On)?", ["On", "Off", "Back"], channel);
                    else
                        llDialog(id, "Do you want to turn automatic dropping of baskets On or Off (Current Setting: Off)?", ["On", "Off", "Back"], channel);
                }
                else if (message == "Owner Only")
                {
                    HttpRequest("/Cat/Settings?breeding=owner&dbid="+(string)dbid,[],"");
                    breeding = breedingowner;
                }
                else if (message == "Group Only")
                {
                    HttpRequest("/Cat/Settings?breeding=group&dbid="+(string)dbid,[],"");
                    breeding = breedinggroup;
                }
                else if (message == "Everyone")
                {
                    HttpRequest("/Cat/Settings?breeding=everyone&dbid="+(string)dbid,[],"");
                    breeding = breedingall;
                }
                else if (message == "Back")
                {
                    showMainMenu();
                }
                else
                {
                    integer k = (integer)message;
                    HttpRequest("/Cat/Settings?breeding=mate&mate="+(string)message +"&dbid="+(string)dbid,[],"");
                    breeding = breedingmate;
                    mate = k;
                }
            }
            else if (question == question_paw_points)
            {
                if(message == "Yes")
                {
                    pawPointsKey = HttpRequest("/Cat/pawpoints?type=cat&dbid="+(string)dbid,[],"");
                    endDialog();
                }
                else if (message == "No")
                {
                    endDialog();
                }
                else if (message == "Back")
                {
                    showMainMenu();
                }
            }
            else if (question == question_settings)
            {
                if (message == "Sounds")
                {
                    llSetTimerEvent(20.0);
                    listenHandle = llListen(channel,"",dialogUser,""); 
                    question = question_sound;
                    if(soundOn)
                        llDialog(id, "Do you want to turn sounds On or Off (Current Setting: On)?" ,["On","Off","Back"],channel);
                    else
                        llDialog(id, "Do you want to turn sounds On or Off (Current Setting: Off)?" ,["On","Off","Back"],channel);
                }
                else if (message == "Hovertext")
                {
                    llSetTimerEvent(20.0);
                    listenHandle = llListen(channel,"",dialogUser,"");    
                    question = question_hovertext;
                    string namestring;
                    string agestring;
                    string needsstring;
                    string breedingstring;
                    
                    if(hovername)
                    namestring = "Name (On)";
                    else
                    namestring = "Name (Off)";
                    if(hoverage)
                    agestring = "Age (On)";
                    else
                    agestring = "Age (Off)";
                    if(hoverneeds)
                    needsstring = "Needs (On)";
                    else
                    needsstring = "Needs (Off)";
                    if(hoverbreeding)
                    breedingstring = "Breeding (On)";
                    else
                    breedingstring = "Breeding (Off)";
					list options = [namestring, agestring];
					if(!ispet)
					{
						options += [needsstring,breedingstring];
					}
					options += ["Enable All", "Disable All", "Back"];
                    llDialog(id,"What part of the hovertext do you want to toggle? (Shows current values)", options,channel);
                }
                else if (message == "Return home")
                {
                    llMessageLinked(LINK_ROOT,1,"returnHome","");
                    moving = 0;
                }
                else if (message == "Back")
                {
                    showMainMenu();
                }
            }
            else if (question == question_hovertext)
            {
                string m;
                if(message=="Name (On)")
                {
                    hovername = 0;
                    m = "hovername=off";
                }
                else if(message=="Name (Off)")
                {
                    hovername = 1;
                    m = "hovername=on";
                } else if(message=="Age (On)")
                {
                    hoverage = 0;
                    m = "hoverage=off";
                }
                else if(message=="Age (Off)")
                {
                    hoverage = 1;
                    m = "hoverage=on";
                } else if(message=="Needs (On)")
                {
                    hoverneeds = 0;
                    m = "hoverneeds=off";
                }
                else if(message=="Needs (Off)")
                {
                    hoverneeds = 1;
                    m = "hoverneeds=on";
                } else if(message=="Breeding (On)")
                {
                    hoverbreeding = 0;
                    m = "hoverbreeding=off";
                }
                else if(message=="Breeding (Off)")
                {
                    hoverbreeding = 1;
                    m = "hoverbreeding=on";
                } else if (message == "Enable All")
                {
                    hoverage = 1;
                    hovername = 1;
                    hoverneeds = 1;
                    hoverbreeding = 1;
                    HttpRequest("/Cat/Settings?hovername=on&hoverage=on&hoverneeds=on&hoverbreeding=on&dbid="+(string)dbid,[],"");
                    llMessageLinked(LINK_ROOT,4,"enableall","");
                } else if (message == "Disable All")
                {
                    hoverage = 0;
                    hovername = 0;
                    hoverneeds = 0;
                    hoverbreeding = 0;
                    HttpRequest("/Cat/Settings?hovername=off&hoverage=off&hoverneeds=off&hoverbreeding=off&dbid="+(string)dbid,[],"");
                    llMessageLinked(LINK_ROOT,4,"disableall","");
                }else if (message == "Back")
                {
                    showMainMenu();
                }
                
                if (message != "Enable All" && message != "Disable All" && message != "Back")
                {
                    llMessageLinked(LINK_ROOT,4,m,"");
                    HttpRequest("/Cat/Settings?"+m,[],"");
                }
            }
            else if (question == question_birth)
            {
                if(message = "On")
                {
                    birthOn = 1;
                    HttpRequest("/Cat/Settings?birth=on&dbid="+(string)dbid,[],"");
                    llMessageLinked(LINK_ROOT,3,"birth=on","");
                    endDialog();
                }
                else if (message = "Off")
                {
                    birthOn = 0;
                    HttpRequest("/Cat/Settings?birth=off&dbid="+(string)dbid,[],"");
                    llMessageLinked(LINK_ROOT,3,"birth=off","");
                    endDialog();
                }
                else if (message = "Back")
                {
                    showMainMenu();
                }
            }
            else if (question==question_movement)
            {
                if (message == "5m")
                {
                    llMessageLinked(LINK_ROOT,1,"range:5","");
                    endDialog();
                }
                else if (message == "10m")
                {
                    llMessageLinked(LINK_ROOT,1,"range:10","");
                    endDialog();
                }
                else if (message == "15m")
                {
                    llMessageLinked(LINK_ROOT,1,"range:15","");
                    endDialog();
                }
                else if (message == "25m")
                {
                    llMessageLinked(LINK_ROOT,1,"range:25","");
                    endDialog();
                }
                else if (message == "Move Off")
                {
                    moving = 0;
                    llMessageLinked(LINK_ROOT,1,"stopMovement","");
                    endDialog();
                }
                else if (message == "Move On")
                {
                    moving = 1;
                    llMessageLinked(LINK_ROOT,1,"startMovement","");
                    endDialog();
                }
                else if (message == "Custom")
                {
                    listenHandle = llListen(channel,"",dialogUser,"");
                    question = question_movement_custom;
                    llTextBox(dialogUser,"Please insert only a whole number greater than zero.", channel);
                }
                else if (message == "Back")
                {
                    showMainMenu();
                }
            }
            else if (question == question_movement_custom)
            {
                if(isInteger(message))
                {
                    llMessageLinked(LINK_ROOT,1,"range:"+message,"");
                    
                }
                else
                {
                    llInstantMessage(dialogUser, "Not a valid input.");
                }
                
                endDialog();
            }
            
        }
    }
    http_response(key request_id, integer status, list metadata, string body)
    {
        if(request_id == healrequest && status == 200)
        {
            sick = 0;
            --healthpacks;
            llMessageLinked(LINK_ROOT,4,"notsick","");
        }
        if(request_id == traitsKey && status == 200)
        {
            llInstantMessage(dialogUser,body);
        }
        else if(request_id == pawPointsKey && status == 200)
        {
            llDie();
        }
    }
    
}