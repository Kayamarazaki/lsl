integer hovername;
integer hoverage;
integer hoverneeds;
integer hoverbreeding;
integer sick = 0;
integer healthpacks = 0;
integer petpotions;
integer ispet = 0;
list adminlist = ["1450dde0-246e-4a2f-98f2-ed5e1a00f7b9","1b85d306-3e4a-45c6-b4a8-d57b8e210b52","104e8f31-4153-47ff-bc57-88b5c38d81e3"];


integer initReady = 0;
integer movementReady = 0;
integer statsReady = 0;

integer question = 0;
integer question_movement = 1;
integer question_movement_custom = 2;
integer question_paw_points = 3;
integer question_settings = 4;
integer question_breeding = 5;
integer question_hovertext = 6;
integer question_birth = 7;
integer question_name = 8 ;
integer question_main = 9;
integer question_sound = 10;

integer babyid;
integer breedingall = 0;
integer breedinggroup = 1;
integer breedingowner = 2;
integer breedingmate = 3;

key dialogUser;
integer channel ;
integer listenHandle;
integer inDialog = 0;

integer birthOn = 0;
integer male = 0;
integer babyReady = 0;
integer soundOn = 1;
integer failed = 0;
integer moving = 0;

endDialog()
{
    inDialog=0;
    question = 0;
    triggerSound();
    llSetTimerEvent(0.0);
}
triggerSound()
{
    if(soundOn)
    {
        llSetTimerEvent(llFrand(30.0) + 30.0);
    }
    else
    {
        llSetTimerEvent(0.0);
    }
}
integer isInteger(string var)
{
    integer i;
    for (i=0;i<llStringLength(var);++i)
    {
        if(!~llListFindList(["1","2","3","4","5","6","7","8","9","0"],[llGetSubString(var,i,i)]))
        {
            return FALSE;
        }
    }
    return TRUE;
}
 integer getAdmin(key toucher)
 {
    integer l = llGetListLength(adminlist);
    integer i;
    for(i = 0; i<l;i++)
    {
        if(llList2Key(adminlist,i)== toucher)
            return 1;
    }
    return 0;
 }

showMainMenu()
{
    llSetTimerEvent(20.0);
    question = question_main;
    listenHandle = llListen(channel, "", dialogUser,"");
    string m = "Feisty Felines Cat Menu";
    list options = [];
    options += ["Traits"];
	if(!ispet)
	{
		options += ["Parents"];
	}
    if(dialogUser == llGetOwner())
    {
		options += ["Change Name", "Set Home", "Movement", "Settings"];
		if(!ispet)
		{
			options += ["Breeding", "Paw Points"];
			if(babyReady)
			{
				m += "\nA baby is ready to be dropped!";
				options += ["Drop Basket"];
			}
			if(sick && healthpacks > 0)
			{
				m += "\nHealthpacks: " + (string)healthpacks;
				options += ["Heal"];
			}
			if(petpotions > 0 && !sick)
			{
				m += "\nPet Potions: " + (string)petpotions;
				options += ["Make Pet"];
			}
		}
    }
    llDialog(dialogUser, m, options, channel);
}
integer readyToInit()
{
    return llGetInventoryType("Feisty Feline") != INVENTORY_NONE && llGetInventoryType("Feisty Felines Basket") != INVENTORY_NONE;
}


reset()
{
	initReady = 0;
	movementReady = 0;
	statsReady = 0;
	llSetTimerEvent(60.0);
	failed = 0;
}

default
{
	on_rez()
	{
		reset();
	}
	state_entry()
	{
		reset();
	}
	
	touch_start(integer num)
    {
        if(failed && getAdmin(llDetectedKey(0)))
        {
            dialogUser = llDetectedKey(0);
            llSetTimerEvent(20.0);
            inDialog = 1;
            listenHandle = llListen(channel,"",dialogUser,"");
            llTextBox(dialogUser, "Please enter the dbid this cat should represent.",channel);  
        }
    }
	
	link_message(integer sender, integer num, string msg, key id)
	{
		if(num = 199)
		{
			statsReady = 1;
			if(statsReady && initReady && movementReady)
			{
				llSetTimerEvent(0.0);
				state initialized;
			}
		}
		else if (num = 299)
		{
			initReady = 1;
			if(statsReady && initReady && movementReady)
			{
				llSetTimerEvent(0.0);
				state initialized;
			}
		}
		else if (num = 399)
		{
			movementReady = 1;
			if(statsReady && initReady && movementReady)
			{
				llSetTimerEvent(0.0);
				state initialized;
			}
		}
	}
	timer()
	{
		if(!failed)
		{
			llSetTimerEvent(0.0);
			llOwnerSay("Feisty Feline hasn't initialized within 60 seconds.\n199 received: " + (string)statsReady + "\n299 received: " + (string)initReady + "\n399 received: " + (string)movementReady +".");
			failed = 1;
		}
		else
		{
			endDialog();
			llListenRemove(listenHandle);
            llInstantMessage(dialogUser,"Dialog expired.");
		}
	}
	
}

state initialized
{
	on_rez(integer params)
    {   
        state default;
    }
	state_entry()
    {
        llAllowInventoryDrop(0);
        llSetObjectDesc((string)dbid);
        llSetObjectName(name);
        triggerSound();
    }
    
    touch_start(integer num)
    {        
        if (!inDialog)
        {
            dialogUser = llDetectedKey(0);
            showMainMenu();
        }
    }
	
	timer()
    {
        if(inDialog)
        {
            endDialog();
            llListenRemove(listenHandle);
            llInstantMessage(dialogUser,"Dialog expired.");
        }
        else
        {
            
            string sound ;
            if(llFrand(1.0) < 0.5)
            {
                sound = llList2String(meows,0);
            }
            else
            {
                sound = llList2String(meows,1);
            }
            llPlaySound( sound ,1.0);
            triggerSound();
        }
    }
	
	listen(integer channel, string name, key id, string message)
    {
        if(id == dialogUser)
		{
			llListenRemove(listenHandle);
			if(dialogUser==llGetOwner())
			{
				if(message == "Settings")
				{
					
				}
				else if(message == "Movement")
				{
				
				}
				else if(message == "Pawpoints")
				{
				
				}
				else if(message == "Help")
				{
				
				}
				
				else if(message == "Heal")
				{
				
				}
				else if(message == "Birth")
				{
				
				}
				else if(message == "Forever Pet")
				{
				
				}
			}
			if(message == "Traits")
			{
				
			}
		}
	}
}