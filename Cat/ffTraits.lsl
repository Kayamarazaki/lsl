key initKey;

string scriptversion = "0.3b";

integer hovername;
integer hoverage;
integer hoverbreeding;
integer hoverneeds;

integer birth;

key mate;

integer dbid;

integer breeding;

integer breedingall = 0;
integer breedinggroup = 1;
integer breedingowner = 2;
integer breedingmate = 3;

integer babyid;
integer initialized;
integer gender = 0;
integer hunger =-1;
integer heat = -1;
integer sick = 0;
integer healthpacks;
integer petpotions;
integer ispet = 0;
integer happiness = -1;
string name;
integer age = -1;
integer cycles;
string cattype;


integer babiesready = 0;
integer male = 1;
integer female = 2;

integer pregnancy = -1;
integer pregnancycooldown = -1;


string scriptKey = "nzaXOCwJMlPyR8lCNMwD";
string hostname = "http://www.feistyfelinesbreedables.com:8080/portal";

setText()
{
    
    string text = "Feisty Felines " + cattype + " Version " + scriptversion + "\n";
    if(hovername)
    {
        if(gender == male)
        {
            text += "♂";
        }
        else
        {
            text += "♀";
        }
        text += name + "\n";
    }
    if(hoverage)
    {
        text += "Age: " + (string)age + "\n";
    }
	if(!ispet)
	{
		if(hoverneeds)
		{    
			text += "Hunger: " + (string)hunger + "%\n";
			text += "Happiness: " + (string)happiness + "%\n";
		}
		if(hoverbreeding)
		{
			text += "Heat: " + (string)heat + "%\n";
			if(pregnancy != -1)
			{
				text += "Pregnancy: " + (string)pregnancy+ "%\n";
			}
			else if (pregnancycooldown != -1)
			{
				text += "Pregnancy Cooldown: " + (string)pregnancycooldown+ " hours\n";
			}
			text += "Cycles Left: " + (string)cycles + "\n";
		}
		vector color;
		if(sick)
		{
			color = <1.0,0.0,0.0>;
		}
		else
		{
			color = <1.0,1.0,1.0>;
		}
		llSetText(text,color,1.0);
	}
}

key HttpRequest(string action, list parameters, string body)
{
    return llHTTPRequest(hostname+action,[HTTP_CUSTOM_HEADER,"Script-Key", scriptKey,HTTP_CUSTOM_HEADER, "Group-id", llList2String(llGetObjectDetails(llGetKey(),[OBJECT_GROUP]),0),HTTP_BODY_MAXLENGTH,16384] + parameters ,body);
}

list getXmlContent(string node, string xml)
{
    list xmlContent = [];
    string st = "<" + node + ">";
    string et = "</" + node + ">";
    
    while(llSubStringIndex(xml,st) != -1 || llSubStringIndex(xml,et) != -1)
    {
        integer s = llSubStringIndex(xml,st) + llStringLength(st);
        integer e = llSubStringIndex(xml,et) -1;
        xmlContent += [llGetSubString(xml,s,e)];
        
        xml = llGetSubString(xml,e + llStringLength(et), llStringLength(xml));
    }
    
    
    return xmlContent;
}

default
{
    on_rez(integer params)
    {
        initialized = 0;
        if(params != 0)
        {
            dbid = params;
        }
		llSetObjectDesc((string)dbid);
    }
    
    state_entry()
    {

    }
    
    http_response(key request_id, integer status, list metadata, string bo)
    {
        if(request_id == initKey && status == 200)
        {           
            list li1 =getXmlContent("Properties",bo);
            if(llGetListLength(li1) > 0)
            {
                string properties = llList2String(li1,0);
                
                li1 = getXmlContent("gender",properties);
                if(llGetListLength(li1)>0)
                {
                    string gen = llList2String(li1,0);
                    if(gen == "Male")
                    {
                        gender = male;
                    }
                    if(gen == "Female")
                    {
                        gender = female;
                    }
                }
                li1 = getXmlContent("age",properties);
                if(llGetListLength(li1)>0)
                {
                    age = (integer)llList2String(li1,0);
                }
                li1 = getXmlContent("birth",properties);
                if(llGetListLength(li1)>0)
                {
                    string s = llList2String(li1,0);
                    if(s=="Off")
                    {
                        birth = 0;
                    }
                    else if (s == "On")
                    {
                        birth = 1;
                    }
                }
                li1 = getXmlContent("hovername",properties);
                if(llGetListLength(li1)>0)
                {
                    string s = llList2String(li1,0);
                    if(s=="Off")
                    {
                        hovername = 0;
                    }
                    else if (s == "On")
                    {
                        hovername = 1;
                    }
                }
                li1 = getXmlContent("hoverage",properties);
                if(llGetListLength(li1)>0)
                {
                    string s = llList2String(li1,0);
                    if(s=="Off")
                    {
                        hoverage = 0;
                    }
                    else if (s == "On")
                    {
                        hoverage = 1;
                    }
                }
                li1 = getXmlContent("hoverneeds",properties);
                if(llGetListLength(li1)>0)
                {
                    string s = llList2String(li1,0);
                    if(s=="Off")
                    {
                        hoverneeds = 0;
                    }
                    else if (s == "On")
                    {
                        hoverneeds = 1;
                    }
                }
                li1 = getXmlContent("hoverbreeding",properties);
                if(llGetListLength(li1)>0)
                {
                    string s = llList2String(li1,0);
                    if(s=="Off")
                    {
                        hoverbreeding = 0;
                    }
                    else if (s == "On")
                    {
                        hoverbreeding = 1;
                    }
                }
                li1 = getXmlContent("breeding",properties);
                if(llGetListLength(li1)>0)
                {
                    string br = llList2String(li1,0);
                    if(br == "All")
                    {
                        breeding=breedingall;
                    }
                    else if(br == "Group")
                    {
                        breeding = breedinggroup;
                    }
                    else if (br == "Owner")
                    {
                        breeding = breedingowner;
                    }
                    else
                    {
                        breeding = breedingmate;
                        mate = (key)br;
                    }
                }
                li1 = getXmlContent("id",properties);
                if(llGetListLength(li1)>0)
                {
                    dbid = (integer)llList2String(li1,0);
                }
                li1 = getXmlContent("hunger",properties);
                if(llGetListLength(li1)>0)
                {
                    hunger = (integer)llList2String(li1,0);
                }
                li1 = getXmlContent("cycles",properties);
                if(llGetListLength(li1)>0)
                {
                    cycles = (integer)llList2String(li1,0);
                }
                li1 = getXmlContent("heat",properties);
                if(llGetListLength(li1)>0)
                {
                    heat = (integer)llList2String(li1,0);
                }
                li1 = getXmlContent("sick",properties);
                if(llGetListLength(li1)>0)
                {
                    if(llList2String(li1,0) == "true")
                    {
                        sick = 1;
                    }
                    else
                    {
                        sick = 0;
                    }
                }
                li1 = getXmlContent("healthpacks",properties);
                if(llGetListLength(li1)>0)
                {
                    healthpacks = (integer)llList2String(li1,0);
                }
				li1 = getXmlContent("petpotions",properties);
                if(llGetListLength(li1)>0)
                {
                    petpotions = (integer)llList2String(li1,0);
                }
				li1 = getXmlContent("ispet",properties);
                if(llGetListLength(li1)>0)
                {
                    if(llList2String(li1,0) == "true")
                    {
                        ispet = 1;
                    }
                    else
                    {
                        ispet = 0;
                    }
                }
                li1 = getXmlContent("happiness",properties);
                if(llGetListLength(li1)>0)
                {
                    happiness = (integer)llList2String(li1,0);
                }
                li1 = getXmlContent("name",properties);
                if(llGetListLength(li1)>0)
                {
                    name= llList2String(li1,0);
                }
                li1 = getXmlContent("cattype",properties);
                if(llGetListLength(li1)>0)
                {
                    cattype= llList2String(li1,0);
                }
                li1 = getXmlContent("babiesready",properties);
                if(llGetListLength(li1)>0)
                {
                    babiesready= 1;
                    babyid=(integer)llList2String(li1,0);
                }
                li1 = getXmlContent("pregnancy",properties);
                if(llGetListLength(li1)>0)
                {
                    pregnancy = (integer)llList2String(li1,0);
                    pregnancycooldown = -1;
                }
                else
                {
                    pregnancy = -1;
                    li1 = getXmlContent("pregnancycooldown",properties);
                    if(llGetListLength(li1)>0)
                    {
                        pregnancycooldown = (integer)llList2String(li1,0);
                    }
                    else
                    {
                        pregnancycooldown = -1;
                    }
                }
                
                
                
                properties = "";
                li1 = getXmlContent("Parameters",bo);
                bo= "";
                string parameters = llList2String(li1,0);
                
                
                if(gender > 0 && hunger > -1&& happiness > -1 && name != "" && age > -1 && cattype != "")
                {
					if(ispet)
					{
						llMessageLinked(LINK_ROOT,100,"pet:" + (string)petpotions,"");
					}
					else
					{
						llMessageLinked(LINK_ROOT,100,"notpet:" + (string)petpotions,"");
					}
                    if(babiesready)
                    {
                        llMessageLinked(LINK_ROOT,3,(string)babyid,"");
                        babiesready = 0;
                    }
                    if(gender == male && heat == 100)
                    {
                        llMessageLinked(LINK_ROOT,3,"breed","");
                    }
                    setText();
                    llMessageLinked(LINK_ROOT,100,"dbid:"+(string)dbid,"");
                    llMessageLinked(LINK_ROOT,100,"name:"+(string)name,"");
                    llMessageLinked(LINK_ROOT,2,"age:"+(string)age,"");
                    llMessageLinked(LINK_ROOT,2,"type:"+cattype,"");
                    llMessageLinked(LINK_ROOT,2,parameters,"");
                    if(!initialized)
                    {
                        llMessageLinked(LINK_ROOT,0,"traitsReady","");
                    }
                    if(hoverbreeding)
                    {
                        llMessageLinked(LINK_ROOT,0,"hoverbreeding=on","");
                    }
                    else
                    {
                        llMessageLinked(LINK_ROOT,0,"hoverbreeding=off","");
                    }
                    if(hovername)
                    {
                        llMessageLinked(LINK_ROOT,0,"hovername=on","");
                    }
                    else
                    {
                        llMessageLinked(LINK_ROOT,0,"hovername=off","");
                    }
                    if(hoverage)
                    {
                        llMessageLinked(LINK_ROOT,0,"hoverage=on","");
                    }
                    else
                    {
                        llMessageLinked(LINK_ROOT,0,"hoverage=off","");
                    }
                    if(hoverneeds)
                    {
                        llMessageLinked(LINK_ROOT,0,"hoverneeds=on","");
                    }
                    else
                    {
                        llMessageLinked(LINK_ROOT,0,"hoverneeds=off","");
                    }
                    if(birth)
                    {
                        llMessageLinked(LINK_ROOT,100,"birth=on","");
                    }
                    else
                    {
                        llMessageLinked(LINK_ROOT,100,"birth=off","");
                    }
                    if(gender == male)
                    {
                        llMessageLinked(LINK_ROOT,0,"gender=male","");
                    }
                    else
                    {
                        llMessageLinked(LINK_ROOT,0,"gender=female","");
                    }
                    if(breeding == breedingall)
                    {
                        llMessageLinked(LINK_ROOT,0,"breeding=all","");
                    }
                    else if (breeding == breedinggroup)
                    {
                        llMessageLinked(LINK_ROOT,0,"breeding=group","");
                    }
                    else if (breeding == breedingowner)
                    {
                        llMessageLinked(LINK_ROOT,0,"breeding=owner","");
                    }else
                    {
                        llMessageLinked(LINK_ROOT,0,"mate=" + (string)mate,"");
                    }    
                    if(sick)
                    {
                        llMessageLinked(LINK_ROOT,0,"sick:" + (string)healthpacks,"");
                    }
                    else
                    {
                        llMessageLinked(LINK_ROOT,0,"notsick:" + (string)healthpacks,"");
                    }
                }
            }
        }
    }
    
    link_message(integer sender, integer num, string msg, key id)
    {
        if(num==4 || num == 100)
        {
            if(msg=="hoverbreeding=off")
            {
                hoverbreeding = 0;
                setText();
            } 
            else if (llGetSubString(msg,0,4)=="dbid:")
            {
                dbid = (integer)llGetSubString(msg,5,llStringLength(msg)-1);
            }
            else if(msg == "init")
            {
                initialized=1;
            }
            else if (msg=="notsick")
            {
                sick = 0;
                setText();
            }
            else if(msg=="hoverbreeding=on")
            {
                hoverbreeding = 1;
                setText();
            }
            else if(msg=="hoverage=off")
            {
                hoverage = 0;
                setText();
            } else if(msg=="hoverage=on")
            {
                hoverage = 1;
                setText();
            }
            else if(msg=="hovername=off")
            {
                hovername = 0;
                setText();
            } else if(msg=="hovername=on")
            {
                hovername = 1;
                setText();
            }
            else if(msg=="hoverneeds=off")
            {
                hoverneeds = 0;
                setText();
            } else if(msg=="hoverneeds=on")
            {
                hoverneeds = 1;
                setText();
            } else if(msg == "enableall")
            {
                hovername = 1;
                hoverneeds = 1;
                hoverage = 1;
                hoverbreeding = 1;
                setText();
            } else if(msg == "disableall")
            {
                hovername = 0;
                hoverneeds = 0;
                hoverage = 0;
                hoverbreeding = 0;
                setText();
            }
            else if (llGetSubString(msg,0,4)=="name:")
            {
                name = llGetSubString(msg,5,llStringLength(msg)-1);
            }
            else if (msg=="go")
            {
                initKey = HttpRequest("/Cat/init?dbid="+(string)dbid,[],"");
                llSetTimerEvent(60.0);
            }
            
        }
    }
    
    timer()
    {
        initKey = HttpRequest("/Cat/init?dbid="+(string)dbid,[],"");
    }
}