integer frame;
integer tick = 0;
rotation defRot;
float range = 0.0;
float stepSize = 0.2;
integer stand = 2;
integer front = 1;
integer back = 0;
vector currentDirection ;
vector homePoint;
integer waiting = 0;

integer ears = 4;
integer currentEarsPos = 0;
list earPositions ;
integer lf = 5;
integer currentLfPos = 2;
list lfPositions ;
integer lr = 7;
integer currentLrPos = 2;
list lrPositions ;
integer rf = 6;
integer currentRfPos = 2;
list rfPositions ;
integer rr 8;
integer currentRrPos = 2;
list rrPositions ;

integer offset = 18;
rotation rootRot;

vector destination;

init()
{
        frame = 0;
        defRot = llEuler2Rot(<0.0,90.0,180.0> * DEG_TO_RAD);
        rotation relative = llGetRot() / defRot;
        vector euler = llRot2Euler(relative);
        float angle = euler.x;
        currentDirection = <llSin(angle),-1 * llCos(angle),0.0>;
}

setPosition(integer prim, integer position)
{
        
    list params = llGetLinkPrimitiveParams(1,[PRIM_ROTATION]);
    rotation rootRot = llList2Rot(params,0);
    if(prim == ears && position < 2 && position > -1)
    {
        integer start = position * offset;
        integer end = ((position + 1) * offset) - 1;
        
        integer k = 3 + position * offset;
        integer j = 5 + position * offset;
        vector pos = llList2Vector(earPositions,j);
        vector euler = llList2Vector(earPositions,k);
        rotation rot = llEuler2Rot(euler * DEG_TO_RAD);
        
        
        llSetLinkPrimitiveParamsFast(prim,llList2List(llListReplaceList(llListReplaceList(earPositions, [rot/defRot/rootRot], k, k),[pos/defRot],j,j),start, end));
        
        
        currentEarsPos = position;        
    }else if (prim==lr && position < 3 && position > -1)   
    {
        integer start = position * offset;
        integer end = ((position + 1) * offset) - 1;
        
        integer k = 3 + position * offset;
        integer j = 5 + position * offset;
        vector pos = llList2Vector(lrPositions,j);
        vector euler = llList2Vector(lrPositions,k);
        rotation rot = llEuler2Rot(euler * DEG_TO_RAD);
        
        llSetLinkPrimitiveParamsFast(prim,llList2List(llListReplaceList(llListReplaceList(lrPositions, [rot/defRot/rootRot], k, k),[pos/defRot],j,j),start, end));
          
        currentLrPos = position;
    }else if (prim==rr && position < 3 && position > -1)
    {
        integer start = position * offset;
        integer end = ((position + 1) * offset) - 1;
        
        integer k = 3 + position * offset;
        integer j = 5 + position * offset;
        vector pos = llList2Vector(rrPositions,j);
        vector euler = llList2Vector(rrPositions,k);
        rotation rot = llEuler2Rot(euler * DEG_TO_RAD);
        llSetLinkPrimitiveParamsFast(prim,llList2List(llListReplaceList(llListReplaceList(rrPositions,[rot/defRot/rootRot], k, k),[pos/defRot],j,j),start, end));
          
        currentRrPos = position;
    }else if (prim==lf && position < 3 && position > -1)
    {
        integer start = position * offset;
        integer end = ((position + 1) * offset) - 1;
        
        integer k = 3 + position * offset;
        integer j = 5 + position * offset;
        vector pos = llList2Vector(lfPositions,j);
        vector euler = llList2Vector(lfPositions,k);
        rotation rot = llEuler2Rot(euler * DEG_TO_RAD);
        llSetLinkPrimitiveParamsFast(prim,llList2List(llListReplaceList(llListReplaceList(lfPositions,[rot/defRot/rootRot], k, k),[pos/defRot],j,j),start, end));
          
        currentLfPos = position;
    }else if (prim==rf && position < 3 && position > -1)
    {
        integer start = position * offset;
        integer end = ((position + 1) * offset) - 1;
        
        integer k = 3 + position * offset;
        integer j = 5 + position * offset;
        vector pos = llList2Vector(rfPositions,j);
        vector euler = llList2Vector(rfPositions,k);
        rotation rot = llEuler2Rot(euler * DEG_TO_RAD);
        llSetLinkPrimitiveParamsFast(prim,llList2List(llListReplaceList(llListReplaceList(rfPositions,[rot/defRot/rootRot], k, k),[pos/defRot],j,j),start, end));
          
        currentRfPos = position;
    }
}

setIdle()
{
    setPosition(rf,stand);
    setPosition(lf,stand);
    setPosition(rr,stand);
    setPosition(lr,stand);
    setPosition(ears,0);
}

switchEars()
{
    if(currentEarsPos==0)
                setPosition(ears,1);
            else
                setPosition(ears,0);
                
}

nextFrame()
{
    
        frame++;
        if(frame == 1)
        {
            setPosition(rf,front);
            setPosition(lf,back);
            setPosition(rr,back);
            setPosition(lr,front);
        }
        if(frame == 2)
        {    
            setPosition(lr,stand);
            setPosition(lf,stand);
            setPosition(rr,stand);
            setPosition(rf,stand);
        }
        if(frame == 3)
        {
            setPosition(rf,back);
            setPosition(lf,front);
            setPosition(rr,front);
            setPosition(lr,back);
        }
        if(frame == 4)
        {    
            setPosition(rf,stand);
            setPosition(lf,stand);
            setPosition(rr,stand);
            setPosition(lr,stand);
            frame = 0;
        if(llFrand(1.0) > 0.92)
        {
            switchEars();
        }
    }
}
    integer randSign()
    {
        if(llFrand(-1.0) + llFrand(1.0)>=0)
            return 1;
        else
            return -1;
    }
    
    findDestination()
    {
        float x = llFrand(range);
        float y = llFrand(llSqrt((range*range)-(x*x)));
        vector direction = <randSign() * x, randSign() * y,0.0>;
        destination = homePoint + direction;
        faceDirection(destination-llGetPos());
    }
    
    faceDirection(vector direction)
    {
        rotation rot = llRotBetween( currentDirection, llVecNorm( direction));
        if(rot.x != 0.0)
        {
            rot.z = 1.0;
            rot.x = 0.0;
            rot.y = 0.0;
        }
    
        llRotLookAt(llGetRot()*rot, 1.0, 0.4);
        currentDirection = llVecNorm( direction);
        
    }
    
    vector getStep()
    {
        vector currentPos = llGetPos();
        if(currentPos == destination)
        {
            findDestination();
        }
        vector step;
        vector distance = destination - currentPos;
        float scale = llVecMag(distance)/stepSize;
        
        if(scale <= 1.0)
        {
            return destination;
        }
        
        step = distance / scale;
            
        return currentPos + step;
    }
default
{
    state_entry()
    {
        
    }
    
    link_message(integer sender, integer num, string msg, key id)
    {
        if(num ==100 && msg == "init")
        {
			destination = llGetPos();
			homePoint = destination;
			init();
            state initialized;
        }
        if(num == 1)
        {
            if(llGetSubString(msg,0,4) == "newz:")
            {
                homePoint.z = (integer)llGetSubString(msg,7,llStringLength(msg)-1);
            }
            else
            {

                list li = llParseString2List(msg,["/"],[]);
                if(llGetListLength(li) > 0)
                {
                    if(llGetSubString(llList2String(li,1),0,1) == "LF")
                    {
                        lfPositions = li;
                        integer len = llGetListLength(lfPositions);
                        integer cn = 0;
                        while(cn * offset < len)
                        {
                            integer o = 0;
                            while(o < offset)
                            {
                                integer k = o + cn * offset;
                                if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
                                {
                                    lfPositions = llListReplaceList(lfPositions,[(integer)llList2String(lfPositions,k)],k,k);
                                }
                                if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
                                {
                                    lfPositions = llListReplaceList(lfPositions,[(vector)llList2String(lfPositions,k)],k,k);
                                }
                                if(o == 17)
                                {
                                    lfPositions = llListReplaceList(lfPositions,[(float)llList2String(lfPositions,k)],k,k);
                                }
                                ++o;
                            }
                            ++cn;
                        }
                    }
                    else if(llGetSubString(llList2String(li,1),0,1) == "RF")
                    {
                        rfPositions = li;
                        integer len = llGetListLength(rfPositions);
                        integer cn = 0;
                        while(cn * offset < len)
                        {
                            integer o = 0;
                            while(o < offset)
                            {
                                integer k = o + cn * offset;
                                if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
                                {
                                    rfPositions = llListReplaceList(rfPositions,[(integer)llList2String(rfPositions,k)],k,k);
                                }
                                if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
                                {
                                    rfPositions = llListReplaceList(rfPositions,[(vector)llList2String(rfPositions,k)],k,k);
                                }
                                if(o == 17)
                                {
                                    rfPositions = llListReplaceList(rfPositions,[(float)llList2String(rfPositions,k)],k,k);
                                }
                                ++o;
                            }
                            ++cn;
                        }
                    }
                    else if(llGetSubString(llList2String(li,1),0,1) == "LR")
                    {
                        lrPositions = li;
                        integer len = llGetListLength(lrPositions);
                        integer cn = 0;
                        while(cn * offset < len)
                        {
                            integer o = 0;
                            while(o < offset)
                            {
                                integer k = o + cn * offset;
                                if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
                                {
                                    lrPositions = llListReplaceList(lrPositions,[(integer)llList2String(lrPositions,k)],k,k);
                                }
                                if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
                                {
                                    lrPositions = llListReplaceList(lrPositions,[(vector)llList2String(lrPositions,k)],k,k);
                                }
                                if(o == 17)
                                {
                                    lrPositions = llListReplaceList(lrPositions,[(float)llList2String(lrPositions,k)],k,k);
                                }
                                ++o;
                            }
                            ++cn;
                        }
                    }
                    else if(llGetSubString(llList2String(li,1),0,1) == "RR")
                    {
                        rrPositions = li;
                        integer len = llGetListLength(rrPositions);
                        integer cn = 0;
                        while(cn * offset < len)
                        {
                            integer o = 0;
                            while(o < offset)
                            {
                                integer k = o + cn * offset;
                                if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
                                {
                                    rrPositions = llListReplaceList(rrPositions,[(integer)llList2String(rrPositions,k)],k,k);
                                }
                                if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
                                {
                                    rrPositions = llListReplaceList(rrPositions,[(vector)llList2String(rrPositions,k)],k,k);
                                }
                                if(o == 17)
                                {
                                    rrPositions = llListReplaceList(rrPositions,[(float)llList2String(rrPositions,k)],k,k);
                                }
                                ++o;
                            }
                            ++cn;
                        }
                    }
                    else if(llGetSubString(llList2String(li,1),0,3) == "Ears")
                    {
                        earPositions = li;
                        integer len = llGetListLength(earPositions);
                        integer cn = 0;
                        while(cn * offset < len)
                        {
                            integer o = 0;
                            while(o < offset)
                            {
                                integer k = o + cn * offset;
                                if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
                                {
                                    earPositions = llListReplaceList(earPositions,[(integer)llList2String(earPositions,k)],k,k);
                                }
                                if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
                                {
                                    earPositions = llListReplaceList(earPositions,[(vector)llList2String(earPositions,k)],k,k);
                                }
                                if(o == 17)
                                {
                                    earPositions = llListReplaceList(earPositions,[(float)llList2String(earPositions,k)],k,k);
                                }
                                ++o;
                            }
                            ++cn;
                        }
                    }
                }
                
            }
            if(rf != 0 && lf != 0 && lr != 0 && rr != 0&& ears != 0 && llGetListLength(earPositions) != 0 && llGetListLength(lfPositions) != 0 && llGetListLength(rfPositions) != 0 && llGetListLength(rrPositions) != 0 && llGetListLength(lrPositions) != 0)
            {
                llMessageLinked(LINK_ROOT,0,"movementReady","");
            }
        }
    }
}

state initialized
{
    on_rez(integer params)
    {
        state default;
    }
    
    state_entry()
    {
        
    }
    link_message(integer sender, integer num, string msg, key id)
    {
        if(num == 1)
        {
            if(msg == "setHome")
            {
                homePoint = llGetPos();
                destination = homePoint;
            }
            else if("range:" == llGetSubString(msg, 0, 5))
            {
                range = (float)llDeleteSubString(msg, 0, 5);
            }
            else if(msg == "startMovement")
            {
                state roaming;
            }
            else if (msg=="returnHome")
            {
                llSetPos(homePoint);
            }
            else if(msg == "continue" && waiting)
            {
                waiting = 0;
                state roaming;
            }
            else if(llGetSubString(msg,0,4) == "newz:")
            {
                homePoint.z = (integer)llGetSubString(msg,7,llStringLength(msg)-1);
            }
            else
            {

                list li = llParseString2List(msg,["/"],[]);
                if(llGetListLength(li) > 0)
                {
                    if(llGetSubString(llList2String(li,1),0,1) == "LF")
                    {
                        lfPositions = li;
                        integer len = llGetListLength(lfPositions);
                        integer cn = 0;
                        while(cn * offset < len)
                        {
                            integer o = 0;
                            while(o < offset)
                            {
                                integer k = o + cn * offset;
                                if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
                                {
                                    lfPositions = llListReplaceList(lfPositions,[(integer)llList2String(lfPositions,k)],k,k);
                                }
                                if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
                                {
                                    lfPositions = llListReplaceList(lfPositions,[(vector)llList2String(lfPositions,k)],k,k);
                                }
                                if(o == 17)
                                {
                                    lfPositions = llListReplaceList(lfPositions,[(float)llList2String(lfPositions,k)],k,k);
                                }
                                ++o;
                            }
                            ++cn;
                        }
                    }
                    else if(llGetSubString(llList2String(li,1),0,1) == "RF")
                    {
                        rfPositions = li;
                        integer len = llGetListLength(rfPositions);
                        integer cn = 0;
                        while(cn * offset < len)
                        {
                            integer o = 0;
                            while(o < offset)
                            {
                                integer k = o + cn * offset;
                                if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
                                {
                                    rfPositions = llListReplaceList(rfPositions,[(integer)llList2String(rfPositions,k)],k,k);
                                }
                                if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
                                {
                                    rfPositions = llListReplaceList(rfPositions,[(vector)llList2String(rfPositions,k)],k,k);
                                }
                                if(o == 17)
                                {
                                    rfPositions = llListReplaceList(rfPositions,[(float)llList2String(rfPositions,k)],k,k);
                                }
                                ++o;
                            }
                            ++cn;
                        }
                    }
                    else if(llGetSubString(llList2String(li,1),0,1) == "LR")
                    {
                        lrPositions = li;
                        integer len = llGetListLength(lrPositions);
                        integer cn = 0;
                        while(cn * offset < len)
                        {
                            integer o = 0;
                            while(o < offset)
                            {
                                integer k = o + cn * offset;
                                if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
                                {
                                    lrPositions = llListReplaceList(lrPositions,[(integer)llList2String(lrPositions,k)],k,k);
                                }
                                if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
                                {
                                    lrPositions = llListReplaceList(lrPositions,[(vector)llList2String(lrPositions,k)],k,k);
                                }
                                if(o == 17)
                                {
                                    lrPositions = llListReplaceList(lrPositions,[(float)llList2String(lrPositions,k)],k,k);
                                }
                                ++o;
                            }
                            ++cn;
                        }
                    }
                    else if(llGetSubString(llList2String(li,1),0,1) == "RR")
                    {
                        rrPositions = li;
                        integer len = llGetListLength(rrPositions);
                        integer cn = 0;
                        while(cn * offset < len)
                        {
                            integer o = 0;
                            while(o < offset)
                            {
                                integer k = o + cn * offset;
                                if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
                                {
                                    rrPositions = llListReplaceList(rrPositions,[(integer)llList2String(rrPositions,k)],k,k);
                                }
                                if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
                                {
                                    rrPositions = llListReplaceList(rrPositions,[(vector)llList2String(rrPositions,k)],k,k);
                                }
                                if(o == 17)
                                {
                                    rrPositions = llListReplaceList(rrPositions,[(float)llList2String(rrPositions,k)],k,k);
                                }
                                ++o;
                            }
                            ++cn;
                        }
                    }
                    else if(llGetSubString(llList2String(li,1),0,3) == "Ears")
                    {
                        earPositions = li;
                        integer len = llGetListLength(earPositions);
                        integer cn = 0;
                        while(cn * offset < len)
                        {
                            integer o = 0;
                            while(o < offset)
                            {
                                integer k = o + cn * offset;
                                if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
                                {
                                    earPositions = llListReplaceList(earPositions,[(integer)llList2String(earPositions,k)],k,k);
                                }
                                if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
                                {
                                    earPositions = llListReplaceList(earPositions,[(vector)llList2String(earPositions,k)],k,k);
                                }
                                if(o == 17)
                                {
                                    earPositions = llListReplaceList(earPositions,[(float)llList2String(earPositions,k)],k,k);
                                }
                                ++o;
                            }
                            ++cn;
                        }
                    }
                }
                init();
            }                
        }
    }
}

state roaming
{
    on_rez(integer params)
    {
		setIdle();
        init();
		state default;
    }
    state_entry()
    {
        tick = 1;
        frame = 0;
        llSetTimerEvent(stepSize);
    }
    
    timer()
    {
        vector vec = llGetPos();
        homePoint.z = vec.z;
        llSetPos(getStep());
        tick++;
        if(tick%2 == 0)
        {
            nextFrame();
            tick = 0;
        }
    }
    link_message(integer sender, integer num, string msg, key id)
    {
        if(num == 1)
        {
            if(msg=="stopMovement")
            {
                llSetTimerEvent(0);
				setIdle();
				init();
                state initialized;
            }
            else if (msg=="returnHome")
            {
                llSetPos(homePoint);
				setIdle();
				init();
                state initialized;
            }
            else if (msg=="wait")
            {
                waiting = 1;
                state initialized;
            }
        }
        
    }
    
}
