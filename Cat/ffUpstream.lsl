string pin = "774542319";
integer channelDie = -992345;
integer channelHud = -894532;

integer listenHandleDie;
integer listenHandleHud;

integer dbid;

list uuids;
list dbids;

string scriptKey = "nzaXOCwJMlPyR8lCNMwD";
string hostname = "http://www.feistyfelinesbreedables.com:8080/portal";

key HttpRequest(string action, list parameters, string body)
{
    return llHTTPRequest(hostname+action,[HTTP_CUSTOM_HEADER,"Script-Key", scriptKey,HTTP_CUSTOM_HEADER, "Group-id", llList2String(llGetObjectDetails(llGetKey(),[OBJECT_GROUP]),0),HTTP_BODY_MAXLENGTH,16384] + parameters ,body);
}

integer isInteger(string var)
{
    integer i;
    for (i=0;i<llStringLength(var);++i)
    {
        if(!~llListFindList(["1","2","3","4","5","6","7","8","9","0"],[llGetSubString(var,i,i)]))
        {
            return FALSE;
        }
    }
    return TRUE;
}

default
{
    on_rez(integer params)
    {
        llResetScript();
    }
    state_entry()
    {
        listenHandleDie = llListen(channelDie,"Feisty Felines Updater","",pin + (string)llGetKey());
        listenHandleHud = llListen(channelHud,"Feisty Felines HUD","","");
    }
    
    listen(integer channel, string name, key id, string message)
    {
        if(message == pin+(string)llGetKey())
        {
            llDie();
        }
        if(name == "Feisty Felines HUD")
        {
            if(isInteger(message))
            {
                llMessageLinked(LINK_ROOT,1,"range:" + message,"");
            }
            else if (message == "breeding=owner")
            {
                HttpRequest("/Cat/Settings?"+message+"&dbid="+(string)dbid,[],"");
                llMessageLinked(LINK_ROOT,100,message,"");
            }
            else if (message == "breeding=group")
            {
                HttpRequest("/Cat/Settings?"+message+"&dbid="+(string)dbid,[],"");
                llMessageLinked(LINK_ROOT,100,message,"");
            }
            else if (message == "breeding=everyone")
            {
                HttpRequest("/Cat/Settings?"+message+"&dbid="+(string)dbid,[],"");
                llMessageLinked(LINK_ROOT,100,message,"");
            }
            else if (message == "birth=on")
            {
                HttpRequest("/Cat/Settings?"+message+"&dbid="+(string)dbid,[],"");
                llMessageLinked(LINK_ROOT,100,message,"");
            }
            else if (message == "birth=off")
            {
                HttpRequest("/Cat/Settings?"+message+"&dbid="+(string)dbid,[],"");
                llMessageLinked(LINK_ROOT,100,message,"");
            }
            else if (message == "home")
            {
                llMessageLinked(LINK_ROOT,1,"setHome","");
            }
        }
    }
    
    link_message(integer sender, integer num, string msg, key id)
    {
        if (llGetSubString(msg,0,4)=="dbid:")
        {
            dbid = (integer)llGetSubString(msg,5,llStringLength(msg)-1);
        }
    }
    
    
}