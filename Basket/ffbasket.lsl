string scriptKey = "nzaXOCwJMlPyR8lCNMwD";
key requestKey;
float height;
string hovertext;
string hostname = "http://www.feistyfelinesbreedables.com:8080/portal";
 list adminlist = ["1450dde0-246e-4a2f-98f2-ed5e1a00f7b9","1b85d306-3e4a-45c6-b4a8-d57b8e210b52","104e8f31-4153-47ff-bc57-88b5c38d81e3"];    
 string pin = "774542319";
integer channelDie = -992345;
 key feistykey = "1450dde0-246e-4a2f-98f2-ed5e1a00f7b9";
 key pawPointsKey;
 
//Menu
key dialogUser;
integer listenHandle;
integer channel ;
integer inDialog = 0;
integer retries=0;
integer dying = 0;
integer generationtype = -1;
integer dbid = 0;
integer listenHandleDie;
integer question = 0;
integer question_paw_points = 1;
integer question_birth = 2;
integer continue = 0;
 integer getAdmin(key toucher)
 {
    integer l = llGetListLength(adminlist);
    integer i;
    for(i = 0; i<l;i++)
    {
        if(llList2Key(adminlist,i)== toucher)
            return 1;
    }
    return 0;
 }

list getXmlContent(string node, string xml)
{
    list xmlContent = [];
    string st = "<" + node + ">";
    string et = "</" + node + ">";
    
    while(llSubStringIndex(xml,st) != -1 || llSubStringIndex(xml,et) != -1)
    {
        integer s = llSubStringIndex(xml,st) + llStringLength(st);
        integer e = llSubStringIndex(xml,et) -1;
        xmlContent += [llGetSubString(xml,s,e)];
        
        xml = llGetSubString(xml,e + llStringLength(et), llStringLength(xml));
    }
    
    
    return xmlContent;
}
key HttpRequest(string action, list parameters, string body)
{
    return llHTTPRequest(hostname+action,[HTTP_CUSTOM_HEADER,"Script-Key", scriptKey,HTTP_CUSTOM_HEADER, "Group-id", llList2String(llGetObjectDetails(llGetKey(),[OBJECT_GROUP]),0),HTTP_BODY_MAXLENGTH,16384] + parameters ,body);
}
integer isInteger(string var)
{
    integer i;
    for (i=0;i<llStringLength(var);++i)
    {
        if(!~llListFindList(["1","2","3","4","5","6","7","8","9","0"],[llGetSubString(var,i,i)]))
        {
            return FALSE;
        }
    }
    return TRUE;
}
setBlanket(string gender)
{
    if(gender=="male")
    {
        llSetLinkPrimitiveParamsFast(2,[PRIM_COLOR,ALL_SIDES,<138.0/255.0,208.0/255.0,232.0/255.0>,1.0]);
    }
    else
    {
        llSetLinkPrimitiveParamsFast(2,[PRIM_COLOR,ALL_SIDES,<1.0,177.0/255.0,216.0/255.0>,1.0]);
    }
}
integer readyToInit()
{
    return llGetInventoryType("Feisty Feline") != INVENTORY_NONE && llGetInventoryType("Feisty Felines Basket") != INVENTORY_NONE;
}
endDialog()
{
    inDialog=0;
    question = 0;
    llSetTimerEvent(0.0);
}
default
{
    on_rez(integer params)
    {
        if(params != 0)
            dbid = params;
		if(dbid == 0)
		{
			llResetScript();
		}
    }
    state_entry()
    {
        llAllowInventoryDrop(1);
		channel = (integer)(llFrand(-5000.0) - 5000.0);
        llSetObjectDesc(llGetKey());
        llSetTimerEvent(1.0);
		listenHandleDie = llListen(channelDie,"Feisty Felines Updater","",pin + (string)llGetKey());
    }
    
    timer()
    {
        retries++;
        if(readyToInit())
        {
            llSetTimerEvent(0.0);
            state init;
        }
        else if (retries == 100)
        {
            llOwnerSay("Initializing failed");
            llSetTimerEvent(0.0);
        }
    }
	
	listen(integer channel, string name, key id, string message)
    {
        if(message == pin+(string)llGetKey())
        {
            llDie();
        }
	}
}


state init
{
    on_rez(integer params)
    {
		if(params != 0)
		{
			dbid = params;
		}
		if(dbid == 0)
		{
			llResetScript();
		}
		else
			state default;
    }
    
    state_entry()
    {
		listenHandleDie = llListen(channelDie,"Feisty Felines Updater","",pin + (string)llGetKey());
        llAllowInventoryDrop(0);
		llSetObjectDesc((string)dbid);
		if(llGetOwner()!=feistykey || continue)
		{
			requestKey = HttpRequest("/Cat/basket?id="+(string)generationtype+"&dbid=" + (string)dbid,[],"");
			continue = 0;
		}
    }
    
	listen(integer channel, string name, key id, string message)
    {
        if(message == pin+(string)llGetKey())
        {
            llDie();
        }
		else
		{
			llListenRemove(listenHandle);
        
			if(isInteger(message))
			{
				dbid = (integer)message;
				continue = 1;
				endDialog();
				state default;
			}
		}
	}
	
	touch_start(integer num)
    {
        if(getAdmin(llDetectedKey(0)))
        {
            dialogUser = llDetectedKey(0);
            llSetTimerEvent(20.0);
            inDialog = 1;
            listenHandle = llListen(channel,"",dialogUser,"");
            llTextBox(dialogUser, "Please enter the dbid this cat should represent.",channel);  
        }
    }
	
	
    http_response(key request_id, integer status, list metadata, string body)
    {
        if(request_id == requestKey && status == 200)
        {
            list li1 = getXmlContent("gender",body);
            if(llGetListLength(li1)>0)
            {
                setBlanket(llList2String(li1,0));
            }
            li1 = getXmlContent("height",body);
            if(llGetListLength(li1)>0)
            {
                height = (float)llList2String(li1,0);
            }
            li1 = getXmlContent("dbid",body);
            if(llGetListLength(li1)>0)
            {
                dbid = (integer)llList2String(li1,0);
				llSetObjectDesc((string)dbid);
            }
			li1 = getXmlContent("hovertext",body);
            if(llGetListLength(li1)>0)
            {
                hovertext = llList2String(li1,0);
            }
            state initialized;
        }
    }
}

state initialized
{
    on_rez(integer params)
    {
        state default;
    }
    state_entry()
    {
		llSetText(hovertext,<1.0,1.0,1.0>,1.0);
        llAllowInventoryDrop(0);
    }
    touch_start(integer num)
    {        
        if(llDetectedKey(0) == llGetOwner() && !inDialog)
        {
            dialogUser = llDetectedKey(0);
            llSetTimerEvent(20.0);
            inDialog = 1;
            listenHandle = llListen(channel, "", dialogUser,"");
            llDialog(dialogUser, "Basket menu: " +(string)dbid, ["Parents", "Traits", "Birth", "Paw Points"], channel);
        }
        else if (!inDialog)
        {
            dialogUser = llDetectedKey(0);
            llSetTimerEvent(20.0);
            inDialog = 1;
            listenHandle = llListen(channel, "", dialogUser,"");
            llDialog(dialogUser, "Basket menu:", ["Parents", "Traits"], channel);
        }
    }
    listen(integer channel, string avatarName, key id, string message)
    {
        llListenRemove(listenHandle);
		if(message == pin+(string)llGetKey())
        {
            llDie();
        }
		else if(dialogUser==llGetOwner())
        {
            if(message == "Birth")
            {
                listenHandle = llListen(channel,"",dialogUser,"");
                question = question_birth;
                llSetTimerEvent(20.0);
                llDialog(dialogUser, "Are you sure you want to birth this cat?", ["Yes","No"], channel);
            }
            else if(message == "Paw Points")
            {
                listenHandle = llListen(channel,"",dialogUser,"");
                question = question_paw_points;
                llSetTimerEvent(20.0);
                llDialog(dialogUser, "Are you sure you want to turn this cat in for Paw Points?", ["Yes","No"], channel);
            }
            else if(question == question_paw_points)
            {
                if(message=="Yes")
                {
					pawPointsKey = HttpRequest("/Cat/pawpoints?type=basket&dbid="+(string)dbid,[],"");
                    endDialog();
                    //PawPoints
                }
                else if (message == "No")
                {
                    endDialog();
                }
            }
            else if (question == question_birth)
            {
                if(message=="Yes")
                {
                    llRezObject("Feisty Feline", llGetPos()+ <0.0,0.0,height>, <0.0,0.0,0.0>, llEuler2Rot(<0.0,90.0,180.0> * DEG_TO_RAD), dbid);
                    endDialog();

                }
                else if (message == "No")
                {
                    endDialog();
                }
            }
        }
        if(message == "Traits")
        {
            requestKey = HttpRequest("/Cat/traits?type=basket&dbid="+(string)dbid,[],"");
            endDialog();
        }
        else if(message == "Parents")
        {
            requestKey = HttpRequest("/Cat/parenttraits?type=basket&dbid="+(string)dbid,[],"");
            endDialog();
        }
    }
    http_response(key request_id, integer status, list metadata, string body)
    {
        if(request_id == requestKey && status == 200)
        {
            llInstantMessage(dialogUser,body);

        }
		if(request_id == pawPointsKey && status == 200)
        {
            llDie();

        }
    }
    
    object_rez(key catid)
    {
        llGiveInventory(catid,"Feisty Felines Basket");
        llGiveInventory(catid,"Feisty Feline");
        llDie();
    }
    
    timer()
    {

            endDialog();
            llListenRemove(listenHandle);
            llInstantMessage(dialogUser,"Dialog expired.");
        
    }
}