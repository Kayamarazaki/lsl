list objectnames;
list amounts;
integer price;
string hostname = "http://www.feistyfelinesbreedables.com:8080/portal"; 
string scriptKey = "nzaXOCwJMlPyR8lCNMwD";
 list acceslist = ["1450dde0-246e-4a2f-98f2-ed5e1a00f7b9","1b85d306-3e4a-45c6-b4a8-d57b8e210b52"];
 
 integer channel;
 integer inDialog = 0;
 key dialogUser ;
 integer listenHandle;
 key requestKey;
 
 integer food;
 
 endDialog()
{
    inDialog=0;
    llSetTimerEvent(0.0);
}
 

key HttpRequest(string action, list parameters, string body)
{
    return llHTTPRequest(hostname+action,[HTTP_CUSTOM_HEADER,"Script-Key", scriptKey,HTTP_CUSTOM_HEADER, "Group-id", llList2String(llGetObjectDetails(llGetKey(),[OBJECT_GROUP]),0),HTTP_BODY_MAXLENGTH,16384] + parameters ,body);
}
 list getXmlContent(string node, string xml)
{
    list xmlContent = [];
    string st = "<" + node + ">";
    string et = "</" + node + ">";
    
    while(llSubStringIndex(xml,st) != -1 || llSubStringIndex(xml,et) != -1)
    {
        integer s = llSubStringIndex(xml,st) + llStringLength(st);
        integer e = llSubStringIndex(xml,et) -1;
        xmlContent += [llGetSubString(xml,s,e)];
        
        xml = llGetSubString(xml,e + llStringLength(et), llStringLength(xml));
    }
    
    
    return xmlContent;
}
 integer getAccess(key toucher)
 {
    integer l = llGetListLength(acceslist);
    integer i;
    for(i = 0; i<l;i++)
    {
        if(llList2Key(acceslist,i)== toucher)
            return 1;
    }
    return 0;
 }
 
 default
 {
    on_rez(integer start)
    {
        llResetScript();
    }
    
    state_entry()
    {
        channel = (integer)(llFrand(-5000.0) - 5000.0);
    }
    
    touch_start(integer num)
    {
        if(getAccess(llDetectedKey(0)))
        {
            inDialog = 1;
            dialogUser = llDetectedKey(0);
            llSetTimerEvent(20.0);
            listenHandle = llListen(channel, "",dialogUser,"");
            llTextBox(dialogUser, "Please input a vendor id.", channel);
        }
    }
    listen( integer channel, string name, key id, string message )
    {
        llListenRemove(listenHandle);
        llSetTimerEvent(0.0);
        integer vendorid = (integer)message;
        requestKey= HttpRequest("/Vendor/getVendor?id="+(string)vendorid,[],"");
            
    }
    
    http_response(key request_id, integer status, list metadata, string body)
    {
        if(request_id == requestKey && status == 200)
        {    
            list prices =getXmlContent("price",body);
            if(llGetListLength(prices)>0)
            {
                price = llList2Integer(prices,0);
                list lines = getXmlContent("VendorLine",body);
                integer len = llGetListLength(lines);
                integer i ;
                for(i=0;i<len;i++)
                {
                    list object = getXmlContent("ObjectName",llList2String(lines,i));
                    list amount = getXmlContent("amount",llList2String(lines,i));
                    if(llGetListLength(object) != 0 &&llGetListLength(amount) != 0)
                    {

                        string oname= llList2String(object,0);
                        integer num = llList2Integer(amount,0);
                        if(llGetInventoryKey(oname) != NULL_KEY && num > 0)
                        {
                            objectnames += oname;
                            amounts += num;
                        }
                    }
                    
                }
            
                if(llGetListLength(objectnames) >0)
                     llRequestPermissions(llGetOwner(), PERMISSION_DEBIT);
                else
                    llOwnerSay("Error loadingvendor. \nStatus: " + (string)status+ "\nBody:\n" + body);
            }
            else
                llOwnerSay("Error loadingvendor. \nStatus: " + (string)status+ "\nBody:\n" + body);
        }
        else
            llOwnerSay("Error loadingvendor. \nStatus: " + (string)status+ "\nBody:\n" + body);
    }
    
    timer()
    {
        endDialog();
        llListenRemove(listenHandle);
        llInstantMessage(dialogUser,"Dialog expired.");
    }
    run_time_permissions(integer perm)
    {
        if(perm & PERMISSION_DEBIT)
        {
            llInstantMessage(dialogUser, "Vendor set up!");
            endDialog();
            state initialized;
        }
    }

 }
 
 state initialized
 {
    state_entry()
    {
        requestKey = NULL_KEY;
        llSetPayPrice(PAY_HIDE, [price,PAY_HIDE,PAY_HIDE,PAY_HIDE]);
    }
    
    money(key id, integer amount)
    {
        if(amount != price)
        {
            llGiveMoney(id, amount);
            llInstantMessage(id, "You paid "+(string)amount+", which is the wrong price, the price is: "+(string)price);
        }
        else
        {
            integer len = llGetListLength(objectnames);
            integer i;
			integer food= 0;
            for(i = 0; i < len; i++)
            {
                integer num = llList2Integer(amounts,i);
                integer j ;
                for(j= 0; j < num; j++)
				{
                    llGiveInventory(id,llList2String(objectnames,i));
				}
				if(llSubStringIndex(llList2String(objectnames,i),"Basket") != -1)
				{
					food += num;
				}
            }
			if(food > 0)
			{
				HttpRequest("/Owner/vendorfood?id=" + (string)id + "&num=" + (string)food,[],"");
				HttpRequest("/Owner/vendorhappiness?id=" + (string)id + "&num=" + (string)food,[],"");
			}
			
            llInstantMessage(id, "Thank you for your purchase!");
        }
    }
 }