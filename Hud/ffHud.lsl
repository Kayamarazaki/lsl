string scriptKey = "nzaXOCwJMlPyR8lCNMwD";
string hostname = "http://www.feistyfelinesbreedables.com:8080/portal";

integer channel ;
integer listenHandle;
integer inDialog = 0;
key dialogUser;

key requestKey;

integer channelHud = -894532;

integer option_website = 4 ;
integer option_social = 5 ;
integer option_pawpoints = 3;
integer option_settings = 2;
integer option_auction = 6;

integer question;
integer question_main = 1;
integer question_birth = 2;
integer question_breeding = 3;
integer question_movement = 4;
integer question_movement_custom = 5;

integer range;

integer breedsettings;
integer owner = 1;
integer group = 2;
integer everyone = 3;

integer birthsettings;
integer on = 1;
integer off = 2;

string toSend;

integer isInteger(string var)
{
    integer i;
    for (i=0;i<llStringLength(var);++i)
    {
        if(!~llListFindList(["1","2","3","4","5","6","7","8","9","0"],[llGetSubString(var,i,i)]))
        {
            return FALSE;
        }
    }
    return TRUE;
}

sendMessage(string body)
{
    list cats = llParseString2List(body,[","],[]);
    integer i = 0;
    integer len = llGetListLength(cats);
    for(;i < len; ++i)
    {
        llRegionSayTo((key)llList2String(cats,i),channelHud,toSend);
    }
}

key HttpRequest(string action, list parameters, string body)
{
    return llHTTPRequest(hostname+action,[HTTP_CUSTOM_HEADER,"Script-Key", scriptKey, HTTP_BODY_MAXLENGTH,16384] + parameters ,body);
}

endDialog()
{
    inDialog=0;
    question = 0;
    llSetTimerEvent(0.0);
}

showBreedingMenu()
{
    llSetTimerEvent(20.0);
    question = question_main;
    listenHandle = llListen(channel, "", dialogUser,"");
    list options = ["Breeding", "Birth", "Movement", "Set Home", "Register"];
    string m = "Feisty Felines Breeding Menu";
    llDialog(dialogUser, m, options, channel);
}

default
{
    state_entry()
    {
        channel = (integer)(llFrand(-5000.0) - 5000.0);
    }
    
    touch_start(integer num)
    {
        if(llDetectedKey(0) == llGetOwner() && !inDialog)
        {
            integer option = llDetectedLinkNumber(0);
            
            if(option == option_website)
            {
                llInstantMessage(llGetOwner(),"\nVisit us at:\nhttp://www.feistyfelinesbreedables.com");
            }
            else if(option == option_social)
            {
                llInstantMessage(llGetOwner(),"\nVisit us on Facebook:\nhttps://www.facebook.com/FeistyFelinesBreedables\nOr Twitter:\nhttps://twitter.com/FeistyFelinesSL");
            }
            else if(option == option_pawpoints)
            {
                state pawpoints;
            }
            else if(option == option_settings)
            {
                dialogUser = llGetOwner();
                question = question_main;
                showBreedingMenu();
            }
            else if (option== option_auction)
            {
                llInstantMessage(llGetOwner(),"\nTo see upcoming auctions, please visit us at:\nhttp://www.feistyfelinesbreedables.com");
            }
        }
    }
    
    listen(integer channel, string name, key id, string message)
    {
        llListenRemove(listenHandle);
        if(message == "Back")
        {
            showBreedingMenu();
        }
        else if(question == question_main)
        {
            if(message == "Birth")
            {
                llSetTimerEvent(20.0);
                listenHandle = llListen(channel,"",dialogUser,"");
                question = question_birth;
                llDialog(dialogUser, "Do you want to turn automatic dropping of baskets On or Off? \nNote: This will apply to all of your cats in the current region.", ["On", "Off", "Back"], channel);
            }
            else if (message == "Register")
            {
                endDialog();
                state register;
            }
            else if (message == "Breeding")
            {
                llSetTimerEvent(20.0);
                listenHandle = llListen(channel,"",dialogUser,"");
                question = question_breeding;
                llDialog(dialogUser, "Do you want your cats to breed with Everyone, Group Only or Owner Only? \nNote: This will apply to all of your cats in the current region.", ["Everyone", "Group Only", "Owner Only", "Back"], channel);
            }
            else if (message == "Movement")
            {
                llSetTimerEvent(20.0);
                listenHandle = llListen(channel,"",dialogUser,"");
                question = question_movement;
                llDialog(dialogUser, "Choose movement range. \nNote: This will apply to all of your cats in the current region.", ["5m","10m","15m","25m", "Custom", "Back"], channel);
                
            }
            else if (message == "Set Home")
            {
                toSend = "home";
                endDialog();
                state send;
            }
        }
        else if (question == question_birth)
        {
            if(message == "On")
            {
                toSend = "birth=on";
                endDialog();
                state send;
            }
            else if (message == "Off")
            {
                toSend = "birth=off";
                endDialog();
                state send;
            }
        }
        else if (question == question_breeding)
        {
            if(message == "Everyone")
            {
                toSend = "breeding=everyone";
                endDialog();
                state send;
            }
            else if (message == "Group Only")
            {
                toSend = "breeding=group";
                endDialog();
                state send;
            }
            else if (message == "Owner Only")
            {
                toSend = "breeding=owner";
                endDialog();
                state send;
            }
        }
        else if (question == question_movement)
        {
            if (message == "5m")
            {
                toSend = "5";
                endDialog();
                state send;
            }
            else if (message == "10m")
            {
                toSend = "10";
                endDialog();
                state send;
            }
            else if (message == "15m")
            {
                toSend = "15";
                endDialog();
                state send;
            }
            else if (message == "25m")
            {
                toSend = "25";
                endDialog();
                state send;
            }
            else if (message == "Custom")
            {
                llSetTimerEvent(20.0);
                listenHandle = llListen(channel,"",dialogUser,"");
                question = question_movement_custom;
                llTextBox(dialogUser,"Please insert only a whole number greater than zero.", channel);
            }
        }
         else if (question == question_movement_custom)
        {
            if(isInteger(message))
            {
                toSend = message;
                endDialog();
                state send;
            }
            else
            {
                llInstantMessage(dialogUser, "Not a valid input.");
                endDialog();
            }
        }
    }
    
    timer()
    {
        endDialog();
        llListenRemove(listenHandle);
        llInstantMessage(dialogUser,"Dialog expired.");
    }
}

state pawpoints
{
    state_entry()
    {
        requestKey = HttpRequest("/Owner/getPawPoints",[],"");
    }
    
    http_response(key request_id, integer status, list metadata, string body)
    {
        if(request_id == requestKey && status == 200)
        {
            llInstantMessage(llGetOwner(),body);
        }
        state default;
    }
}
state register
{
    state_entry()
    {
        requestKey = HttpRequest("/Owner/getRegisterKey",[],"");
    }
    
    http_response(key request_id, integer status, list metadata, string body)
    {
        if(request_id == requestKey)
        {
            if(status == 200)
            {
                llInstantMessage(llGetOwner(),body);
            }
            
            state default;
        }
    }
}
state send
{
    state_entry()
    {
        requestKey = HttpRequest("/Owner/catsinregion",[],"");
    }
    
    http_response(key request_id, integer status, list metadata, string body)
    {
        if(request_id == requestKey)
        {
            if(status == 200)
            {
                sendMessage(body);
            }
            
            state default;
        }
    }
}