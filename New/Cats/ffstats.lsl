string cattype;
string scriptversion;
integer hovername;
integer initialized = 0;
integer hoverage;
integer hoverneeds;
integer hoverbreeding;
integer ispet;
integer gender;
string name;
integer age;
integer pregnancy;
integer pregnancycooldown;
integer sick;
integer heat;
integer hunger;
integer happiness;
integer cycles;

integer male = 1;
integer female = 2;

setText()
{
    
    string text = "Feisty Felines " + cattype + " Version " + scriptversion + "\n";
    if(hovername)
    {
        if(gender == male)
        {
            text += "♂";
        }
        else
        {
            text += "♀";
        }
        text += name + "\n";
    }
    if(hoverage)
    {
        text += "Age: " + (string)age + "\n";
    }
	if(!ispet)
	{
		if(hoverneeds)
		{    
			text += "Hunger: " + (string)hunger + "%\n";
			text += "Happiness: " + (string)happiness + "%\n";
		}
		if(hoverbreeding)
		{
			text += "Heat: " + (string)heat + "%\n";
			if(pregnancy != -1)
			{
				text += "Pregnancy: " + (string)pregnancy+ "%\n";
			}
			else if (pregnancycooldown != -1)
			{
				text += "Pregnancy Cooldown: " + (string)pregnancycooldown+ " hours\n";
			}
			text += "Cycles Left: " + (string)cycles + "\n";
		}
		vector color;
		if(sick)
		{
			color = <1.0,0.0,0.0>;
		}
		else
		{
			color = <1.0,1.0,1.0>;
		}
		llSetText(text,color,1.0);
	}
}

default
{
	on_rez(integer params)
	{
		initialized = 0;
	}
	link_message(integer sender, integer num, string msg, key id)
	{
		if(num == 100)
		{
			setText();
			if(!intialized)
			{
				llMessageLinked(LINK_ROOT,199,"","");
				initialized = 1;
			}
		}
		else if(num == 101)
		{
			gender = (integer)msg;
		}
		else if(num == 102)
		{
			age = (integer)msg;
		}
		else if(num == 104)
		{
			hovername = (integer)msg;
		}
		else if(num == 105)
		{
			hoverage = (integer)msg;
		}
		else if(num == 106)
		{
			hoverneeds = (integer)msg;
		}
		else if(num == 107)
		{
			hoverbreeding = (integer)msg;
		}
		else if(num == 110)
		{
			hunger = (integer)msg;
		}
		else if(num == 111)
		{
			happiness = (integer)msg;
		}
		else if(num == 112)
		{
			heat = (integer)msg;
		}
		else if(num == 113)
		{
			cycles = (integer)msg;
		}
		else if(num == 114)
		{
			sick = (integer)msg;
		}
		else if(num == 117)
		{
			ispet = (integer)msg;
		}
		else if(num == 118)
		{
			cattype = msg;
		}
		else if(num == 121)
		{
			pregnancy = (integer)msg;
		}
		else if(num == 122)
		{
			pregnancycooldown = (integer)msg;
		}
		else if(num == 123)
		{
			name = 	msg;
		}
	}
}