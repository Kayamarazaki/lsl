string types;
string sculptmap;
string pos;
string size;
string rot;
string mirror;
string stitching;
string texture;
float height1;
float height2;
float height3;

reset()
{
	types = "";
	sculptmap = "";
	pos = "";
	size = "";
	rot = "";
	mirror = "";
    stitching = "";
	texture = "";
    height1 = -1.0;
    height2 = -1.0;
    height3 = -1.0;
}

string str_replace(string str, string search, string replace) {
    return llDumpList2String(llParseStringKeepNulls((str = "") + str, [search], []), replace);
}

string getXmlContent(string node, string xml)
{
	string xmlContent = "";
	string st = "<" + node + ">";
	string et = "</" + node + ">";
	
	if(llSubStringIndex(xml,st) != -1 || llSubStringIndex(xml,et) != -1)
	{
		integer s = llSubStringIndex(xml,st) + llStringLength(st);
		integer e = llSubStringIndex(xml,et) -1;
		xmlContent = llGetSubString(xml,s,e);
	}  
	
	return xmlContent;
}


default
{
	link_message(integer sender, integer num, string msg, key id)
	{
		if(num == 2)
		{
			reset();
			string val = getXmlContent("types",msg);
			if(val != "")
			{
				types = val;
			}
			val = getXmlContent("texture",msg);
			if(val!="")
			{
				texture = val;
			}
			val = getXmlContent("sculptmap",msg);
			if(val!="")
			{
				sculptmap = val;
			}
			val = getXmlContent("pos",msg);
			if(val != "")
			{
				val = str_replace(val,"&lt;","<");
				val= str_replace(val,"&gt;",">");
				pos = val;
			}
			val = getXmlContent("size",msg);
			if(val != "")
			{
				val = str_replace(val,"&lt;","<");
				val= str_replace(val,"&gt;",">");
				size = val;
			}
			val = getXmlContent("rot",msg);
			if(val != "")
			{
				val = str_replace(val,"&lt;","<");
				val= str_replace(val,"&gt;",">");
				rot = val;
			}
			val = getXmlContent("mirror",msg);
			if(val != "")
			{
				mirror = val;
			}
			val = getXmlContent("stitching",msg);
			if(val != "")
			{
				stitching = val;
			}
			val = getXmlContent("height1",msg);
			if(val != "")
			{
				height1 = (float)val;
			}
			val = getXmlContent("height2",msg);
			if(val != "")
			{
				height2 = (float)val;
			}
			val = getXmlContent("height3",msg);
			if(val != "")
			{
				height3 = (float)val;
			}
			
			if(types == "")
			{
				llOwnerSay("Error while parsing parameters element. Error code: P201");
			}
			else if(sculptmap == "")
			{
				llOwnerSay("Error while parsing parameters element. Error code: P202");
			}
			else if(pos == "")
			{
				llOwnerSay("Error while parsing parameters element. Error code: P203");
			}
			else if(size == "")
			{
				llOwnerSay("Error while parsing parameters element. Error code: P204");
			}
			else if(rot == "")
			{
				llOwnerSay("Error while parsing parameters element. Error code: P205");
			}
			else if(mirror == "")
			{
				llOwnerSay("Error while parsing parameters element. Error code: P206");
			}
			else if(stitching == "")
			{
				llOwnerSay("Error while parsing parameters element. Error code: P207");
			}
			else if(texture == "")
			{
				llOwnerSay("Error while parsing parameters element. Error code: P208");
			}
			else if(height1 == -1.0 && height2 == -1.0 && height3 == -1.0)
			{
				llOwnerSay("Error while parsing parameters element. Error code: P209");
			}
			else
			{
				llMessageLinked(LINK_ROOT,201,types,"");
				llMessageLinked(LINK_ROOT,202,sculptmap,"");
				llMessageLinked(LINK_ROOT,203,pos,"");
				llMessageLinked(LINK_ROOT,204,size,"");
				llMessageLinked(LINK_ROOT,205,rot,"");
				llMessageLinked(LINK_ROOT,206,mirror,"");
				llMessageLinked(LINK_ROOT,207,stitching,"");
				llMessageLinked(LINK_ROOT,208,texture,"");
				llMessageLinked(LINK_ROOT,209,(string)height1,"");
				llMessageLinked(LINK_ROOT,210,(string)height2,"");
				llMessageLinked(LINK_ROOT,211,(string)height3,"");
				llMessageLinked(LINK_ROOT,200,"","");
			}
		}
	}
}