integer frame;
integer tick = 0;
rotation defRot;
float range = 0.0;
float stepSize = 0.2;
integer stand = 2;
integer front = 1;
integer back = 0;
integer initialized = 0;
vector currprimentDirection ;
vector homePoint;
integer waiting = 0;

integer earsprim = 4;
integer currprimentearsprimPos = 0;
list earPositions ;
integer lfprim = 5;
integer currprimentlfprimPos = 2;
list lfprimPositions ;
integer lrprim = 7;
integer currprimentlrprimPos = 2;
list lrprimPositions ;
integer rfprim = 6;
integer currprimentrfprimPos = 2;
list rfprimPositions ;
integer rrprim 8;
integer currprimentrrprimPos = 2;
list rrprimPositions ;

integer offset = 18;
rotation rootRot;

vector destination;

init()
{
        frame = 0;
        defRot = llEuler2Rot(<0.0,90.0,180.0> * DEG_TO_RAD);
        rotation relative = llGetRot() / defRot;
        vector euler = llrprimot2Euler(relative);
        float angle = euler.x;
        currprimentDirection = <llSin(angle),-1 * llCos(angle),0.0>;
}

setPosition(integer prim, integer position)
{
        
    list params = llGetLinkPrimitiveParams(1,[PRIM_ROTATION]);
    rotation rootRot = llList2Rot(params,0);
    if(prim == earsprim && position < 2 && position > -1)
    {
        integer start = position * offset;
        integer end = ((position + 1) * offset) - 1;
        
        integer k = 3 + position * offset;
        integer j = 5 + position * offset;
        vector pos = llList2Vector(earPositions,j);
        vector euler = llList2Vector(earPositions,k);
        rotation rot = llEuler2Rot(euler * DEG_TO_RAD);
        
        
        llSetLinkPrimitiveParamsFast(prim,llList2List(llListReplaceList(llListReplaceList(earPositions, [rot/defRot/rootRot], k, k),[pos/defRot],j,j),start, end));
        
        
        currprimentearsprimPos = position;        
    }else if (prim==lrprim && position < 3 && position > -1)   
    {
        integer start = position * offset;
        integer end = ((position + 1) * offset) - 1;
        
        integer k = 3 + position * offset;
        integer j = 5 + position * offset;
        vector pos = llList2Vector(lrprimPositions,j);
        vector euler = llList2Vector(lrprimPositions,k);
        rotation rot = llEuler2Rot(euler * DEG_TO_RAD);
        
        llSetLinkPrimitiveParamsFast(prim,llList2List(llListReplaceList(llListReplaceList(lrprimPositions, [rot/defRot/rootRot], k, k),[pos/defRot],j,j),start, end));
          
        currprimentlrprimPos = position;
    }else if (prim==rrprim && position < 3 && position > -1)
    {
        integer start = position * offset;
        integer end = ((position + 1) * offset) - 1;
        
        integer k = 3 + position * offset;
        integer j = 5 + position * offset;
        vector pos = llList2Vector(rrprimPositions,j);
        vector euler = llList2Vector(rrprimPositions,k);
        rotation rot = llEuler2Rot(euler * DEG_TO_RAD);
        llSetLinkPrimitiveParamsFast(prim,llList2List(llListReplaceList(llListReplaceList(rrprimPositions,[rot/defRot/rootRot], k, k),[pos/defRot],j,j),start, end));
          
        currprimentrrprimPos = position;
    }else if (prim==lfprim && position < 3 && position > -1)
    {
        integer start = position * offset;
        integer end = ((position + 1) * offset) - 1;
        
        integer k = 3 + position * offset;
        integer j = 5 + position * offset;
        vector pos = llList2Vector(lfprimPositions,j);
        vector euler = llList2Vector(lfprimPositions,k);
        rotation rot = llEuler2Rot(euler * DEG_TO_RAD);
        llSetLinkPrimitiveParamsFast(prim,llList2List(llListReplaceList(llListReplaceList(lfprimPositions,[rot/defRot/rootRot], k, k),[pos/defRot],j,j),start, end));
          
        currprimentlfprimPos = position;
    }else if (prim==rfprim && position < 3 && position > -1)
    {
        integer start = position * offset;
        integer end = ((position + 1) * offset) - 1;
        
        integer k = 3 + position * offset;
        integer j = 5 + position * offset;
        vector pos = llList2Vector(rfprimPositions,j);
        vector euler = llList2Vector(rfprimPositions,k);
        rotation rot = llEuler2Rot(euler * DEG_TO_RAD);
        llSetLinkPrimitiveParamsFast(prim,llList2List(llListReplaceList(llListReplaceList(rfprimPositions,[rot/defRot/rootRot], k, k),[pos/defRot],j,j),start, end));
          
        currprimentrfprimPos = position;
    }
}

setIdle()
{
    setPosition(rfprim,stand);
    setPosition(lfprim,stand);
    setPosition(rrprim,stand);
    setPosition(lrprim,stand);
    setPosition(earsprim,0);
}

switchearsprim()
{
    if(currprimentearsprimPos==0)
                setPosition(earsprim,1);
            else
                setPosition(earsprim,0);
                
}

nextFrame()
{
    
        frame++;
        if(frame == 1)
        {
            setPosition(rfprim,front);
            setPosition(lfprim,back);
            setPosition(rrprim,back);
            setPosition(lrprim,front);
        }
        if(frame == 2)
        {    
            setPosition(lrprim,stand);
            setPosition(lfprim,stand);
            setPosition(rrprim,stand);
            setPosition(rfprim,stand);
        }
        if(frame == 3)
        {
            setPosition(rfprim,back);
            setPosition(lfprim,front);
            setPosition(rrprim,front);
            setPosition(lrprim,back);
        }
        if(frame == 4)
        {    
            setPosition(rfprim,stand);
            setPosition(lfprim,stand);
            setPosition(rrprim,stand);
            setPosition(lrprim,stand);
            frame = 0;
        if(llfprimrand(1.0) > 0.92)
        {
            switchearsprim();
        }
    }
}
integer randSign()
{
	if(llfprimrand(-1.0) + llfprimrand(1.0)>=0)
		return 1;
	else
		return -1;
}

findDestination()
{
	float x = llfprimrand(range);
	float y = llfprimrand(llSqrt((range*range)-(x*x)));
	vector direction = <randSign() * x, randSign() * y,0.0>;
	destination = homePoint + direction;
	faceDirection(destination-llGetPos());
}

faceDirection(vector direction)
{
	rotation rot = llrprimotBetween( currprimentDirection, llVecNorm( direction));
	if(rot.x != 0.0)
	{
		rot.z = 1.0;
		rot.x = 0.0;
		rot.y = 0.0;
	}

	llrprimotLookAt(llGetRot()*rot, 1.0, 0.4);
	currprimentDirection = llVecNorm( direction);
	
}

vector getStep()
{
	vector currprimentPos = llGetPos();
	if(currprimentPos == destination)
	{
		findDestination();
	}
	vector step;
	vector distance = destination - currprimentPos;
	float scale = llVecMag(distance)/stepSize;
	
	if(scale <= 1.0)
	{
		return destination;
	}
	
	step = distance / scale;
		
	return currprimentPos + step;
}

default
{
    on_rez(integer num)
	{
		initialized = 0;
	}
	
	
    state_entry()
    {
		destination = llGetPos();
		homePoint = destination;
    }
    link_message(integer sender, integer num, string msg, key id)
    {
		if(num == 304)
		{
			list li = llParseString2List(msg,["/"],[]);
			if(llGetListLength(li) > 0)
			{
				if(llGetSubString(llList2String(li,1),0,1) == "lfprim")
				{
					lfprimPositions = li;
					integer len = llGetListLength(lfprimPositions);
					integer cn = 0;
					while(cn * offset < len)
					{
						integer o = 0;
						while(o < offset)
						{
							integer k = o + cn * offset;
							if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
							{
								lfprimPositions = llListReplaceList(lfprimPositions,[(integer)llList2String(lfprimPositions,k)],k,k);
							}
							if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
							{
								lfprimPositions = llListReplaceList(lfprimPositions,[(vector)llList2String(lfprimPositions,k)],k,k);
							}
							if(o == 17)
							{
								lfprimPositions = llListReplaceList(lfprimPositions,[(float)llList2String(lfprimPositions,k)],k,k);
							}
							++o;
						}
						++cn;
					}
				}
				else if(llGetSubString(llList2String(li,1),0,1) == "rfprim")
				{
					rfprimPositions = li;
					integer len = llGetListLength(rfprimPositions);
					integer cn = 0;
					while(cn * offset < len)
					{
						integer o = 0;
						while(o < offset)
						{
							integer k = o + cn * offset;
							if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
							{
								rfprimPositions = llListReplaceList(rfprimPositions,[(integer)llList2String(rfprimPositions,k)],k,k);
							}
							if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
							{
								rfprimPositions = llListReplaceList(rfprimPositions,[(vector)llList2String(rfprimPositions,k)],k,k);
							}
							if(o == 17)
							{
								rfprimPositions = llListReplaceList(rfprimPositions,[(float)llList2String(rfprimPositions,k)],k,k);
							}
							++o;
						}
						++cn;
					}
				}
				else if(llGetSubString(llList2String(li,1),0,1) == "lrprim")
				{
					lrprimPositions = li;
					integer len = llGetListLength(lrprimPositions);
					integer cn = 0;
					while(cn * offset < len)
					{
						integer o = 0;
						while(o < offset)
						{
							integer k = o + cn * offset;
							if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
							{
								lrprimPositions = llListReplaceList(lrprimPositions,[(integer)llList2String(lrprimPositions,k)],k,k);
							}
							if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
							{
								lrprimPositions = llListReplaceList(lrprimPositions,[(vector)llList2String(lrprimPositions,k)],k,k);
							}
							if(o == 17)
							{
								lrprimPositions = llListReplaceList(lrprimPositions,[(float)llList2String(lrprimPositions,k)],k,k);
							}
							++o;
						}
						++cn;
					}
				}
				else if(llGetSubString(llList2String(li,1),0,1) == "rrprim")
				{
					rrprimPositions = li;
					integer len = llGetListLength(rrprimPositions);
					integer cn = 0;
					while(cn * offset < len)
					{
						integer o = 0;
						while(o < offset)
						{
							integer k = o + cn * offset;
							if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
							{
								rrprimPositions = llListReplaceList(rrprimPositions,[(integer)llList2String(rrprimPositions,k)],k,k);
							}
							if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
							{
								rrprimPositions = llListReplaceList(rrprimPositions,[(vector)llList2String(rrprimPositions,k)],k,k);
							}
							if(o == 17)
							{
								rrprimPositions = llListReplaceList(rrprimPositions,[(float)llList2String(rrprimPositions,k)],k,k);
							}
							++o;
						}
						++cn;
					}
				}
				else if(llGetSubString(llList2String(li,1),0,3) == "earsprim")
				{
					earPositions = li;
					integer len = llGetListLength(earPositions);
					integer cn = 0;
					while(cn * offset < len)
					{
						integer o = 0;
						while(o < offset)
						{
							integer k = o + cn * offset;
							if(o == 0 || o == 2 || o == 4 || o == 6 || o == 8 || o == 9 || o == 11 || o == 12 || o == 13)
							{
								earPositions = llListReplaceList(earPositions,[(integer)llList2String(earPositions,k)],k,k);
							}
							if(o== 3 || o == 5  || o == 7  || o == 15  || o == 16)
							{
								earPositions = llListReplaceList(earPositions,[(vector)llList2String(earPositions,k)],k,k);
							}
							if(o == 17)
							{
								earPositions = llListReplaceList(earPositions,[(float)llList2String(earPositions,k)],k,k);
							}
							++o;
						}
						++cn;
					}
				}
			}
			init();
			if(!initialized)
			{
				llMessageLinked(LINK_ROOT,399,"","");
				initialized = 1;
			}
			
		}
		else if(num == 302)
		{
			waiting = 0;
            state roaming;
		}
		else if(num == 310)
		{
			homePoint.z = (float)msg;
		}
		else if(num == 305)
		{
			state roaming;
		}
		else if(num == 307)
		{
            llSetPos(homePoint);
		}
		else if(num == 308)
		{
			homePoint = llGetPos();
            destination = homePoint;
		}
		else if(num == 309)
		{
			range = (float)msg;
		}
		else if(num == 301)
		{
			homePoint.z = (float)msg;
		}
    }
}

state roaming
{
    on_rez(integer params)
    {
		setIdle();
        init();
		initialized = 0;
		state default;
    }
    state_entry()
    {
        tick = 1;
        frame = 0;
        llSetTimerEvent(stepSize);
    }
    
    timer()
    {
        vector vec = llGetPos();
        homePoint.z = vec.z;
        llSetPos(getStep());
        tick++;
        if(tick%2 == 0)
        {
            nextFrame();
            tick = 0;
        }
    }
    link_message(integer sender, integer num, string msg, key id)
    {
		if(num == 306)
		{
			llSetTimerEvent(0);
			setIdle();
			init();
			state default;
		}
		else if (num == 307)
		{
			llSetPos(homePoint);
			setIdle();
			init();
			state default;
		}
		else if (num == 303)
		{
			waiting = 1;
			state default;
		}
    }
    
}
