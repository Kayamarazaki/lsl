string scriptKey = "nzaXOCwJMlPyR8lCNMwD";
string hostname = "http://www.feistyfelinesbreedables.com:8080/portal";
key initKey;
integer dbid;

key HttpRequest(string action, list parameters, string body)
{
    return llHTTPRequest(hostname+action,[HTTP_CUSTOM_HEADER,"Script-Key", scriptKey,HTTP_CUSTOM_HEADER, "Group-id", llList2String(llGetObjectDetails(llGetKey(),[OBJECT_GROUP]),0),HTTP_BODY_MAXLENGTH,16384] + parameters ,body);
}

check()
{
	initKey = HttpRequest("/Cat/init?dbid="+(string)dbid,[],"");
    llSetTimerEvent(60.0);
}
list getXmlContent(string node, string xml)
{
    list xmlContent = [];
    string st = "<" + node + ">";
    string et = "</" + node + ">";
    
    while(llSubStringIndex(xml,st) != -1 || llSubStringIndex(xml,et) != -1)
    {
        integer s = llSubStringIndex(xml,st) + llStringLength(st);
        integer e = llSubStringIndex(xml,et) -1;
        xmlContent += [llGetSubString(xml,s,e)];
        
        xml = llGetSubString(xml,e + llStringLength(et), llStringLength(xml));
    }
    
    
    return xmlContent;
}
default
{
	on_rez(integer params)
	{
		if(params != 0)
        {
            dbid = params;
        }
		llSetObjectDesc((string)dbid);
	}
	
	http_response(key request_id, integer status, list metadata, string bo)
	{
		if(request_id == initKey && status == 200)
		{
			list li1 =getXmlContent("Properties",bo);
			if(llGetListLength(li1) > 0)
			{
				string properties = llList2String(li1,0);
				llMessageLinked(LINK_ROOT,1,properties,"");
				
				properties = "";
				li1 = getXmlContent("Parameters",bo);
                if(llGetListLength(li1) > 0)
				{
					string parameters = llList2String(li1,0);
					llMessageLinked(LINK_ROOT,2,parameters,"");
				}
				else
				{
					llOwnerSay("Error while parsing the response from the server. Error code: P200");
				}
			}
			else
			{
				llOwnerSay("Error while parsing the response from the server. Error code: P100");
			}
		}
		else
		{
			llOwnerSay("Error retrieving data from server. HTTP status code: " + (string)status);
		}
	}
	
	state_entry()
	{
		check();
	}
	
	timer()
	{
		check();
	}
	
}