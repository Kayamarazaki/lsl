integer gender = -1;
integer age = -1;
integer birth = -1;
integer hovername = -1;
integer hoverage = -1;
integer hoverneeds = -1;
integer hoverbreeding = -1;
integer breeding = -1;
key mate = NULL_KEY;
integer dbid;
integer hunger = -1;
integer happiness = -1;
integer heat = -1;
integer cycles = -1;
integer sick = -1;
integer healthpacks = -1;
integer petpotions = -1;
integer ispet = -1;
string name = "";
string cattype = "";
integer babiesready = -1;
integer babyid = -1;
integer pregnancy = -2;
integer pregnancycooldown = -2;

integer breedingall = 0;
integer breedinggroup = 1;
integer breedingowner = 2;
integer breedingmate = 3;

integer male = 1;
integer female = 2;


reset()
{
	gender = -1;
	age = -1;
	birth = -1;
	hovername = -1;
	hoverage = -1;
	hoverneeds = -1;
	hoverbreeding = -1;
	breeding = -1;
	mate = NULL_KEY;
	hunger = -1;
	happiness = -1;
	heat = -1;
	cycles = -1;
	sick = -1;
	healthpacks = -1;
	petpotions = -1;
	ispet = -1;
	name = "";
	cattype = "";
	babiesready = -1;
	babyid = -1;
	pregnancy = -2;
	pregnancycooldown = -2;
}

list getXmlContent(string node, string xml)
{
    list xmlContent = [];
    string st = "<" + node + ">";
    string et = "</" + node + ">";
    
    while(llSubStringIndex(xml,st) != -1 || llSubStringIndex(xml,et) != -1)
    {
        integer s = llSubStringIndex(xml,st) + llStringLength(st);
        integer e = llSubStringIndex(xml,et) -1;
        xmlContent += [llGetSubString(xml,s,e)];
        
        xml = llGetSubString(xml,e + llStringLength(et), llStringLength(xml));
    }
    
    
    return xmlContent;
}

default
{
	link_message(integer sender, integer num, string properties, key id)
	{
		if(num == 1)
		{
			reset();
			li1 = getXmlContent("gender",properties);
			if(llGetListLength(li1)>0)
			{
				string gen = llList2String(li1,0);
				if(gen == "Male")
				{
					gender = male;
				}
				if(gen == "Female")
				{
					gender = female;
				}
			}
			li1 = getXmlContent("age",properties);
			if(llGetListLength(li1)>0)
			{
				age = (integer)llList2String(li1,0);
			}
			li1 = getXmlContent("birth",properties);
			if(llGetListLength(li1)>0)
			{
				string s = llList2String(li1,0);
				if(s=="Off")
				{
					birth = 0;
				}
				else if (s == "On")
				{
					birth = 1;
				}
			}
			li1 = getXmlContent("hovername",properties);
			if(llGetListLength(li1)>0)
			{
				string s = llList2String(li1,0);
				if(s=="Off")
				{
					hovername = 0;
				}
				else if (s == "On")
				{
					hovername = 1;
				}
			}
			li1 = getXmlContent("hoverage",properties);
			if(llGetListLength(li1)>0)
			{
				string s = llList2String(li1,0);
				if(s=="Off")
				{
					hoverage = 0;
				}
				else if (s == "On")
				{
					hoverage = 1;
				}
			}
			li1 = getXmlContent("hoverneeds",properties);
			if(llGetListLength(li1)>0)
			{
				string s = llList2String(li1,0);
				if(s=="Off")
				{
					hoverneeds = 0;
				}
				else if (s == "On")
				{
					hoverneeds = 1;
				}
			}
			li1 = getXmlContent("hoverbreeding",properties);
			if(llGetListLength(li1)>0)
			{
				string s = llList2String(li1,0);
				if(s=="Off")
				{
					hoverbreeding = 0;
				}
				else if (s == "On")
				{
					hoverbreeding = 1;
				}
			}
			li1 = getXmlContent("breeding",properties);
			if(llGetListLength(li1)>0)
			{
				string br = llList2String(li1,0);
				if(br == "All")
				{
					breeding=breedingall;
				}
				else if(br == "Group")
				{
					breeding = breedinggroup;
				}
				else if (br == "Owner")
				{
					breeding = breedingowner;
				}
				else
				{
					breeding = breedingmate;
					mate = (key)br;
				}
			}
			li1 = getXmlContent("id",properties);
			if(llGetListLength(li1)>0)
			{
				dbid = (integer)llList2String(li1,0);
			}
			li1 = getXmlContent("hunger",properties);
			if(llGetListLength(li1)>0)
			{
				hunger = (integer)llList2String(li1,0);
			}
			li1 = getXmlContent("cycles",properties);
			if(llGetListLength(li1)>0)
			{
				cycles = (integer)llList2String(li1,0);
			}
			li1 = getXmlContent("heat",properties);
			if(llGetListLength(li1)>0)
			{
				heat = (integer)llList2String(li1,0);
			}
			li1 = getXmlContent("sick",properties);
			if(llGetListLength(li1)>0)
			{
				if(llList2String(li1,0) == "true")
				{
					sick = 1;
				}
				else
				{
					sick = 0;
				}
			}
			li1 = getXmlContent("healthpacks",properties);
			if(llGetListLength(li1)>0)
			{
				healthpacks = (integer)llList2String(li1,0);
			}
			li1 = getXmlContent("petpotions",properties);
			if(llGetListLength(li1)>0)
			{
				petpotions = (integer)llList2String(li1,0);
			}
			li1 = getXmlContent("ispet",properties);
			if(llGetListLength(li1)>0)
			{
				if(llList2String(li1,0) == "true")
				{
					ispet = 1;
				}
				else
				{
					ispet = 0;
				}
			}
			li1 = getXmlContent("happiness",properties);
			if(llGetListLength(li1)>0)
			{
				happiness = (integer)llList2String(li1,0);
			}
			li1 = getXmlContent("name",properties);
			if(llGetListLength(li1)>0)
			{
				name= llList2String(li1,0);
			}
			li1 = getXmlContent("cattype",properties);
			if(llGetListLength(li1)>0)
			{
				cattype= llList2String(li1,0);
			}
			li1 = getXmlContent("babiesready",properties);
			if(llGetListLength(li1)>0)
			{
				babiesready= 1;
				babyid=(integer)llList2String(li1,0);
			}
			else
			{
				babiesready = 0;
			}
			li1 = getXmlContent("pregnancy",properties);
			if(llGetListLength(li1)>0)
			{
				pregnancy = (integer)llList2String(li1,0);
				pregnancycooldown = -1;
			}
			else
			{
				pregnancy = -1;
				li1 = getXmlContent("pregnancycooldown",properties);
				if(llGetListLength(li1)>0)
				{
					pregnancycooldown = (integer)llList2String(li1,0);
				}
				else
				{
					pregnancycooldown = -1;
				}
			}
			
			if(gender == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P101");
			}
			else if(age == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P102");
			}
			else if(birth == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P103");
			}
			else if(hovername == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P104");
			}
			else if(hoverage == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P105");
			}
			else if(hoverneeds == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P106");
			}
			else if(hoverbreeding == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P107");
			}
			else if(breeding == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P108");
			}
			else if(breeding == breedingmate && mate == NULL_KEY)
			{
				llOwnerSay("Error while parsing properties element. Error code: P109");
			}
			else if(hunger == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P110");
			}
			else if(happiness == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P111");
			}
			else if(heat == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P112");
			}
			else if(cycles == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P113");
			}
			else if(sick == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P114");
			}
			else if(healthpacks == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P115");
			}
			else if(petpotions == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P116");
			}
			else if(ispet == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P117");
			}
			else if(cattype == "")
			{
				llOwnerSay("Error while parsing properties element. Error code: P118");
			}
			else if(babiesready == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P119");
			}
			else if(babiesready == 1 && babyid == -1)
			{
				llOwnerSay("Error while parsing properties element. Error code: P120");
			}
			else if(pregnancy == -2)
			{
				llOwnerSay("Error while parsing properties element. Error code: P121");
			}
			else if(pregnancycooldown == -2)
			{
				llOwnerSay("Error while parsing properties element. Error code: P122");
			}
			else
			{
				llMessageLinked(LINK_ROOT,101,(string)gender,"");
				llMessageLinked(LINK_ROOT,102,(string)age,"");
				llMessageLinked(LINK_ROOT,103,(string)birth,"");
				llMessageLinked(LINK_ROOT,104,(string)hovername,"");
				llMessageLinked(LINK_ROOT,105,(string)hoverage,"");
				llMessageLinked(LINK_ROOT,106,(string)hoverneeds,"");
				llMessageLinked(LINK_ROOT,107,(string)hoverbreeding,"");
				llMessageLinked(LINK_ROOT,108,(string)breeding,"");
				llMessageLinked(LINK_ROOT,109,(string)mate,"");
				llMessageLinked(LINK_ROOT,110,(string)hunger,"");
				llMessageLinked(LINK_ROOT,111,(string)happiness,"");
				llMessageLinked(LINK_ROOT,112,(string)heat,"");
				llMessageLinked(LINK_ROOT,113,(string)cycles,"");
				llMessageLinked(LINK_ROOT,114,(string)sick,"");
				llMessageLinked(LINK_ROOT,115,(string)healthpacks,"");
				llMessageLinked(LINK_ROOT,116,(string)petpotions,"");
				llMessageLinked(LINK_ROOT,117,(string)ispet,"");
				llMessageLinked(LINK_ROOT,118,cattype,"");
				llMessageLinked(LINK_ROOT,119,(string)babiesready,"");
				llMessageLinked(LINK_ROOT,120,(string)babyid,"");
				llMessageLinked(LINK_ROOT,121,(string)pregnancy,"");
				llMessageLinked(LINK_ROOT,122,(string)pregnancycooldown,"");
				llMessageLinked(LINK_ROOT,123,name,"");
				llMessageLinked(LINK_ROOT,100,"","");
			}
		}
	}
}