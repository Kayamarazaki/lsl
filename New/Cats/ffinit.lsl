integer age = -1;
integer prevAge = -1;
integer paramsreceived = FALSE;

integer initialized = FALSE;

list types;
list sculptmap;
list pos;
list size;
list rot;
list mirror;
list stitching;
list texture;
float height1;
float height2;
float height3;

integer bodyprim = 1;
integer headprim = 2;
integer eyesprim = 3;
integer earsprim = 4;
integer lfprim = 5;
integer rfprim = 6;
integer lrprim = 7;
integer rrprim = 8;
integer tailprim = 9;
integer neckprim = 10;
integer whiskersprim = 11;

list getSimpleParamList(integer num, string type)
{
    vector p= (vector)llList2String(pos,num);
    vector s= (vector)llList2String(size,num);
    integer mirr = llList2Integer(mirror,num);
    integer stitch = llList2Integer(stitching,num);
    integer sculpttype = 0;
    if(mirr)
    {
        if(stitch)
        {
            sculpttype = PRIM_SCULPT_TYPE_PLANE | PRIM_SCULPT_FLAG_MIRROR;
        }
        else
        {
            sculpttype = PRIM_SCULPT_TYPE_SPHERE | PRIM_SCULPT_FLAG_MIRROR;
        }
    }
    else
    {
        if(stitch)
        {
            sculpttype = PRIM_SCULPT_TYPE_PLANE;
        }
        else
        {
            sculpttype = PRIM_SCULPT_TYPE_SPHERE ;
        }
    }
    return [PRIM_NAME, type, PRIM_ROTATION, (vector)llList2String(rot,num), PRIM_POSITION, p, PRIM_SIZE, s, PRIM_TYPE, PRIM_TYPE_SCULPT, llList2String(sculptmap,num), sculpttype,PRIM_TEXTURE,ALL_SIDES,llList2String(texture,num),<1.0,1.0,0.0>,<0.0,0.0,0.0>,0.0];
}
integer getListNumber(list li, string str)
{
    integer i;
    integer l = llGetListLength(li);
    for(i =0;i<l;i++)
    {
        if(llList2String(li,i) == str)
            return i;
    }
    return -1;
}

setPrim(integer prim, string type)
{
    integer num = getListNumber(types,type);
    if(num != -1)
    {
        list params;
        vector euler = (vector)llList2String(rot,num);
        rotation ro = llEuler2Rot(euler * DEG_TO_RAD);
        vector p= (vector)llList2String(pos,num);
        vector s= (vector)llList2String(size,num);
        string primname;
        string sculptm = llList2String(sculptmap,num);
        string text = llList2String(texture,num);
        integer mirr = llList2Integer(mirror,num);
        integer stitch = llList2Integer(stitching,num);
        
        integer sculpttype = 0;
        if(mirr)
        {
            if(stitch)
            {
                sculpttype = PRIM_SCULPT_TYPE_PLANE | PRIM_SCULPT_FLAG_MIRROR;
            }
            else
            {
                sculpttype = PRIM_SCULPT_TYPE_SPHERE | PRIM_SCULPT_FLAG_MIRROR;
            }
        }
        else
        {
            if(stitch)
            {
                sculpttype = PRIM_SCULPT_TYPE_PLANE;
            }
            else
            {
                sculpttype = PRIM_SCULPT_TYPE_SPHERE ;
            }
        }
        if(prim==1)
        {
            primname = "Feisty Felines " + cattype;
            vector currentPos = llGetPos();
            currentPos.z = currentPos.z + height;
            p = currentPos;
            llMessageLinked(LINK_ROOT,301, (string)p.z,"");
        }
        else
        {
            primname = type;
            vector rootEuler = (vector)llList2String(rot,getListNumber(types,"Body"));
            rotation rootRot = llEuler2Rot(rootEuler * DEG_TO_RAD);
            ro = ro / rootRot / rootRot;
            p = p/rootRot;
        }
        
        list parameters = [PRIM_NAME, primname, PRIM_ROTATION, ro, PRIM_POSITION, p, PRIM_SIZE, s, PRIM_TYPE, PRIM_TYPE_SCULPT, sculptm, sculpttype,PRIM_TEXTURE,ALL_SIDES,text,<1.0,1.0,0.0>,<0.0,0.0,0.0>,0.0];
        
        llSetLinkPrimitiveParamsFast(prim,parameters);
        
    }

}
messagePrimParameter(string type)
{
    list params;
    if(llStringLength(type) == 2)
    {
        integer num = getListNumber(types, type + " Back");
        
        if(num != -1)
        {
            params += getSimpleParamList(num,type);
        }
        num = getListNumber(types, type + " Front");
        
        if(num != -1)
        {
            params += getSimpleParamList(num,type);
        }
        num = getListNumber(types, type + " Stand");
        
        if(num != -1)
        {
            params += getSimpleParamList(num,type);
        }
    
    }
    else if (type = "Ears")
    {
        integer num = getListNumber(types, type + " Upright");
        if(num != -1)
        {
            params += getSimpleParamList(num,type);
        }
        num = getListNumber(types, type + " Drooped");
        if(num != -1)
        {
            params += getSimpleParamList(num,type);
        }
    }    
    llMessageLinked(LINK_ROOT,304,llDumpList2String(params,"/"),"");
}

useParameters()
{
    llMessageLinked(LINK_ROOT,303,"wait","");
	llSleep(1.0);
    setPrim(bodyprim,"Body");
    setPrim(neckprim,"Neck");
    setPrim(headprim,"Head");
    setPrim(eyesprim,"Eyes");
    setPrim(earsprim,"Ears Upright");
    setPrim(lfprim,"LF Stand");
    setPrim(rfprim,"RF Stand");
    setPrim(lrprim,"LR Stand");
    setPrim(rrprim,"RR Stand");
    setPrim(tailprim,"Tail");
    setPrim(whiskersprim,"Whiskers");
    messagePrimParameters();
    
    types = [];
    sculptmap = [];
    pos=[];
    size=[];
    rot=[];
    mirror=[];
    stitching=[];
    texture =[];
    llMessageLinked(LINK_ROOT,302,"continue","");
}

messagePrimParameters()
{
    messagePrimParameter("LF");
    messagePrimParameter("RF");
    messagePrimParameter("LR");
    messagePrimParameter("RR");
    messagePrimParameter("Ears");
}

doInit()
{
	if(age < 3)
	{
		llMessageLinked(LINK_ROOT,401,(string)height1,"");
	}
	else if (age < 5)
	{
		llMessageLinked(LINK_ROOT,401,(string)height2,"");
	}
	else
	{
		llMessageLinked(LINK_ROOT,401,(string)height3,"");
	}
	if(prevAge != age)
	{
		if(prevAge == -1)
		{
			height = 0;
			useParameters();
			paramsset = 1;
		}else if(prevAge < 3)
		{
			if(age >= 5)
			{
				height = height3 - height1;
				useParameters();
				paramsset = 1;
			}
			else if(age >=3)
			{
				height = height2 - height1;
				useParameters();
				paramsset = 1;
			}
		}
		else if(prevAge < 5 && age >= 5)
		{
			height = height3 - height2;
		}
		
	}
	if(!initialized)
	{
		if(!paramsset)
		{
			height = 0;
			useParameters();
		}
	}
	
	prevAge = age;
	age = -1;
	paramsreceived = FALSE;
}

default
{
	on_rez(integer params)
    {
        initialized = FALSE;
    }
	
	link_message(integer sender, integer num, string msg, key id)
	{
		if(num == 201)
		{
			types = llParseString2List(msg,["/"],[]);
		}
		else if(num == 202)
		{
			sculptmap = llParseString2List(msg,["/"],[]);
		}
		else if(num == 203)
		{
			pos = llParseString2List(msg,["/"],[]);
		}
		else if(num == 204)
		{
			size = llParseString2List(msg,["/"],[]);
		}
		else if(num == 205)
		{
			rot = llParseString2List(msg,["/"],[]);
		}
		else if(num == 206)
		{
			mirror = llParseString2List(msg,["/"],[]);
		}
		else if(num == 207)
		{
			stitching = llParseString2List(msg,["/"],[]);
		}
		else if(num == 208)
		{
			texture = llParseString2List(msg,["/"],[]);
		}
		else if(num == 209)
		{
			height1 = (float)msg;
		}
		else if(num == 210)
		{
			height2 = (float)msg;
		}
		else if(num == 211)
		{
			height3 = (float)msg;
		}
		else if(num == 102)
		{
			age = (integer)msg;
			if(paramsreceived)
			{
				doInit();
			}
		}
		else if(num == 200)
		{
			paramsreceived = TRUE;
			if(age != -1)
			{
				doInit();
			}
			if(!intialized)
			{
				initialized = TRUE;
				llMessageLinked(LINK_ROOT,299,"","");
			}
		}
	}
}