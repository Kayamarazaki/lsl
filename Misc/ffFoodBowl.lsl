string scriptKey = "nzaXOCwJMlPyR8lCNMwD";
string hostname = "http://www.feistyfelinesbreedables.com:8080/portal";
key requestKey ;

key requestFood()
{
    return HttpRequest("/Owner/food",[],"");
}

key HttpRequest(string action, list parameters, string body)
{
    return llHTTPRequest(hostname+action,[HTTP_CUSTOM_HEADER,"Script-Key", scriptKey,HTTP_CUSTOM_HEADER, "Group-id", llList2String(llGetObjectDetails(llGetKey(),[OBJECT_GROUP]),0),HTTP_BODY_MAXLENGTH,16384] + parameters ,body);
}

setText(string bo)
{
    integer food = (integer)bo ;
    if(food <= 0)
    {
        llSetText("Your food bowl is empty.",<1.0,1.0,1.0>,1.0);
    }
    else
    {
        llSetText("Food: " + (string)food,<1.0,1.0,1.0>,1.0);
    }
}

default
{
    on_rez(integer params)
    {
        llResetScript();
    }
    state_entry()
    {
        requestKey = requestFood();
        llSetTimerEvent(300.0);
    }
    
    http_response(key request_id, integer status, list metadata, string body)
    {
        if(request_id == requestKey && status == 200)
        {
            setText(body);
        }
    }
    
    timer()
    {
        requestKey = requestFood();
    }
}